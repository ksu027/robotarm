#ifndef ALPHABET_H
#define ALPHABET_H

#include <core/containers/gmdvector.h>
#include <scene/gmscene.h>

class Alphabet
{
public:
  Alphabet(){}
  ~Alphabet(){}

  std::vector<GMlib::Vector<float,3>> getLetterCoordinates(char search_letter, GMlib::Vector<float,3> start_point,float scale = 1.0f);
  std::vector<GMlib::Vector<float,3>> getTextCoordinates(std::string search_text, GMlib::Vector<float,3> start_point, float scale = 1.0f);

private:
  const float m_width = 30.0f;
  const float m_height = 50.0f;

  const float m_w = 25.0f;
  const float m_h = 50.0f;
  const float m_y = 30.0f;
  const float m_o = 0.0f;
  std::map<char,std::vector<GMlib::Vector<float,3>>> m_letters =
  {
      {'h',std::vector<GMlib::Vector<float,3>>{{m_h,m_y,0.0f},{m_h,0.0f,0.0f},{0.0f,0.0f,0.0f},{0.0f,m_y,0.0f},{m_h,m_y,m_w},{m_h,0.0f,m_w},{0.0f,0.0f,m_w},{0.0f,m_y,m_w},{m_h/2.0f,m_y,m_w+m_o},{m_h/2.0f,0.0f,m_w+m_o},{m_h/2.0f,0.0f,0.0f-m_o},{m_h/2.0f,m_y,0.0f-m_o}}},
      {'H',std::vector<GMlib::Vector<float,3>>{{m_h,m_y,0.0f},{m_h,0.0f,0.0f},{0.0f,0.0f,0.0f},{0.0f,m_y,0.0f},{m_h,m_y,m_w},{m_h,0.0f,m_w},{0.0f,0.0f,m_w},{0.0f,m_y,m_w},{m_h/2.0f,m_y,m_w+m_o},{m_h/2.0f,0.0f,m_w+m_o},{m_h/2.0f,0.0f,0.0f-m_o},{m_h/2.0f,m_y,0.0f-m_o}}},
      {'e',std::vector<GMlib::Vector<float,3>>{{m_h,m_y,0.0f},{m_h,0.0f,0.0f},{0.0f,0.0f,0.0f},{0.0f,m_y,0.0f},{m_h,m_y,m_w+m_o},{m_h,0.0f,m_w+m_o},{m_h,0.0f,0.0f-m_o},{m_h,m_y,0.0f-m_o},{m_h/2.0f,m_y,m_w+m_o},{m_h/2.0f,0.0f,m_w+m_o},{m_h/2.0f,0.0f,0.0f-m_o},{m_h/2.0f,m_y,0.0f-m_o},{0.0f,m_y,m_w+m_o},{0.0f,0.0f,m_w+m_o},{0.0f,0.0f,0.0f-m_o},{0.0f,m_y,0.0f-m_o}}},
      {'E',std::vector<GMlib::Vector<float,3>>{{m_h,m_y,0.0f},{m_h,0.0f,0.0f},{0.0f,0.0f,0.0f},{0.0f,m_y,0.0f},{m_h,m_y,m_w+m_o},{m_h,0.0f,m_w+m_o},{m_h,0.0f,0.0f-m_o},{m_h,m_y,0.0f-m_o},{m_h/2.0f,m_y,m_w+m_o},{m_h/2.0f,0.0f,m_w+m_o},{m_h/2.0f,0.0f,0.0f-m_o},{m_h/2.0f,m_y,0.0f-m_o},{0.0f,m_y,m_w+m_o},{0.0f,0.0f,m_w+m_o},{0.0f,0.0f,0.0f-m_o},{0.0f,m_y,0.0f-m_o}}},
      {'l',std::vector<GMlib::Vector<float,3>>{{m_h,m_y,0.0f},{m_h,0.0f,0.0f},{0.0f,0.0f,0.0f},{0.0f,m_y,0.0f},{0.0f,m_y,m_w+m_o},{0.0f,0.0f,m_w+m_o},{0.0f,0.0f,0.0f-m_o},{0.0f,m_y,0.0f-m_o}}},
      {'L',std::vector<GMlib::Vector<float,3>>{{m_h,m_y,0.0f},{m_h,0.0f,0.0f},{0.0f,0.0f,0.0f},{0.0f,m_y,0.0f},{0.0f,m_y,m_w+m_o},{0.0f,0.0f,m_w+m_o},{0.0f,0.0f,0.0f-m_o},{0.0f,m_y,0.0f-m_o}}},
      //simple o, as square, could be changed as parametric
      {'o',std::vector<GMlib::Vector<float,3>>{{m_h,m_y,0.0f},{m_h,0.0f,0.0f},{0.0f,0.0f,0.0f},{0.0f,m_y,0.0f},{m_h,m_y,m_w},{m_h,0.0f,m_w},{0.0f,0.0f,m_w},{0.0f,m_y,m_w},{m_h,m_y,m_w+m_o},{m_h,0.0f,m_w+m_o},{m_h,0.0f,0.0f-m_o},{m_h,m_y,0.0f-m_o},{0.0f,m_y,m_w+m_o},{0.0f,0.0f,m_w+m_o},{0.0f,0.0f,0.0f-m_o},{0.0f,m_y,0.0f-m_o}}},
      {'O',std::vector<GMlib::Vector<float,3>>{{m_h,m_y,0.0f},{m_h,0.0f,0.0f},{0.0f,0.0f,0.0f},{0.0f,m_y,0.0f},{m_h,m_y,m_w},{m_h,0.0f,m_w},{0.0f,0.0f,m_w},{0.0f,m_y,m_w},{m_h,m_y,m_w+m_o},{m_h,0.0f,m_w+m_o},{m_h,0.0f,0.0f-m_o},{m_h,m_y,0.0f-m_o},{0.0f,m_y,m_w+m_o},{0.0f,0.0f,m_w+m_o},{0.0f,0.0f,0.0f-m_o},{0.0f,m_y,0.0f-m_o}}},
      {'U',std::vector<GMlib::Vector<float,3>>{{m_h,m_y,0.0f},{m_h,0.0f,0.0f},{0.0f,0.0f,0.0f},{0.0f,m_y,0.0f},{m_h,m_y,m_w},{m_h,0.0f,m_w},{0.0f,0.0f,m_w},{0.0f,m_y,m_w},{0.0f,m_y,m_w+m_o},{0.0f,0.0f,m_w+m_o},{0.0f,0.0f,0.0f-m_o},{0.0f,m_y,0.0f-m_o}}},
      {'u',std::vector<GMlib::Vector<float,3>>{{m_h,m_y,0.0f},{m_h,0.0f,0.0f},{0.0f,0.0f,0.0f},{0.0f,m_y,0.0f},{m_h,m_y,m_w},{m_h,0.0f,m_w},{0.0f,0.0f,m_w},{0.0f,m_y,m_w},{0.0f,m_y,m_w+m_o},{0.0f,0.0f,m_w+m_o},{0.0f,0.0f,0.0f-m_o},{0.0f,m_y,0.0f-m_o}}},
      {'I',std::vector<GMlib::Vector<float,3>>{{m_h,m_y,m_w/2.0f},{m_h,0.0f,m_w/2.0f},{0.0f,0.0f,m_w/2.0f},{0.0f,m_y,m_w/2.0f}}},
      {'i',std::vector<GMlib::Vector<float,3>>{{m_h,m_y,m_w/2.0f},{m_h,0.0f,m_w/2.0f},{0.0f,0.0f,m_w/2.0f},{0.0f,m_y,m_w/2.0f}}},
      {'T',std::vector<GMlib::Vector<float,3>>{{m_h,m_y,m_w/2.0f},{m_h,0.0f,m_w/2.0f},{0.0f,0.0f,m_w/2.0f},{0.0f,m_y,m_w/2.0f},{m_h,m_y,m_w+m_o},{m_h,0.0f,m_w+m_o},{m_h,0.0f,0.0f-m_o},{m_h,m_y,0.0f-m_o}}},
      {'t',std::vector<GMlib::Vector<float,3>>{{m_h,m_y,m_w/2.0f},{m_h,0.0f,m_w/2.0f},{0.0f,0.0f,m_w/2.0f},{0.0f,m_y,m_w/2.0f},{m_h,m_y,m_w+m_o},{m_h,0.0f,m_w+m_o},{m_h,0.0f,0.0f-m_o},{m_h,m_y,0.0f-m_o}}},
      ///signs
    {'!',std::vector<GMlib::Vector<float,3>>{{100.0f,0.0f,25.0f},{25.0f,0.0f,25.0f},{25.0f,15.0f,25.0f},{0.0f,15.0f,0.0f},{0.0f,0.0f,25.0f}}},
    {'_',std::vector<GMlib::Vector<float,3>>{{0.0f,0.0f,0.0f},{0.0f,0.0f,m_w}}},
    {' ',std::vector<GMlib::Vector<float,3>>{{m_h,m_y,0.0f},{m_h,m_y,1.0f}}}
    ///to be continued
  };
};

#endif // ALPHABET_H
