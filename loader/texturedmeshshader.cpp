#include "texturedmeshshader.h"

#include <opengl/gmopenglmanager.h>

using sources = GMlib::GL::OpenGLManager;

TexturedMeshShader::TexturedMeshShader()
{
    std::string vs_str =
            sources::glslDefHeaderVersionSource() +
            "uniform mat4 u_mvmat, u_mvpmat;\n"
            "\n"
            "in vec4 in_vertex;\n"
            "in vec2 in_tex_coord;\n"
            "in vec4 in_normal;\n"
            "\n"
            "out vec4 gl_Position;\n"
            "\n"
            "smooth out vec3 ex_pos;\n"
            "smooth out vec2 ex_tex_coord;\n"
            "smooth out vec3 ex_normal;\n"
            "\n"
            "void main() {\n"
            "\n"
            "  // Transform the normal to view space\n"
            "  mat3 nmat = inverse( transpose( mat3( u_mvmat ) ) );\n"
            "  ex_normal = nmat * vec3(in_normal);\n"
            "\n"
            "  // Transform position into view space;\n"
            "  vec4 v_pos = u_mvmat * in_vertex;\n"
            "  ex_pos = v_pos.xyz * v_pos.w;\n"
            "\n"
            "  ex_tex_coord = in_tex_coord;\n"
            "\n"
            "  // Compute vertex position\n"
            "  gl_Position = u_mvpmat * in_vertex;\n"
            "}\n"
            ;

    std::string fs_str =
            sources::glslDefHeaderVersionSource() +
            sources::glslFnComputeBlinnPhongLightingSource() +

            "uniform mat4      u_mvmat;\n"
            "\n"
            "uniform vec4      u_mat_amb;\n"
            "uniform vec4      u_mat_dif;\n"
            "uniform vec4      u_mat_spc;\n"
            "uniform float     u_mat_shi;\n"
            "uniform sampler2D u_tex;\n"
            "\n"
            "smooth in vec3    ex_pos;\n"
            "smooth in vec2    ex_tex_coord;\n"
            "smooth in vec3    ex_normal;\n"
            "\n"
            "out vec4 gl_FragColor;\n"
            "\n"
            "void main() {\n"
            "\n"
            "  vec3 normal = normalize( ex_normal );\n"
            "\n"
            "  Material mat;\n"
            "  mat.ambient   = u_mat_amb;\n"
            "  mat.diffuse   = u_mat_dif;\n"
            "  mat.specular  = u_mat_spc;\n"
            "  mat.shininess = u_mat_shi;\n"
            "\n"
            "  gl_FragColor = texture(u_tex, ex_tex_coord) * computeBlinnPhongLighting( mat, ex_pos, normal );\n"
            "}\n"
            ;

    GMlib::GL::VertexShader vs;
    vs.create("textured_mesh_vs");
    vs.setPersistent(true);
    vs.setSource(vs_str);
    bool compile_ok = vs.compile();
    if(!compile_ok) {
        std::cerr << "Src:" << std::endl << vs.getSource() << std::endl << std::endl;
        std::cerr << "Error: " << vs.getCompilerLog() << std::endl;
    }
    assert(compile_ok);

    GMlib::GL::FragmentShader fs;
    fs.create("textured_mesh_fs");
    fs.setPersistent(true);
    fs.setSource(fs_str);
    compile_ok = fs.compile();
    if(!compile_ok) {
        std::cerr << "Src:" << std::endl << fs.getSource() << std::endl << std::endl;
        std::cerr << "Error: " << fs.getCompilerLog() << std::endl;
    }
    assert(compile_ok);

    _prog.create("textured_mesh");
    _prog.attachShader(vs);
    _prog.attachShader(fs);
    _prog.setPersistent(true);
    bool link_ok = _prog.link();
    if( !link_ok ) {
      std::cerr << "Error: " << _prog.getLinkerLog() << std::endl;
    }
    assert(link_ok);
}
