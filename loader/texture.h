#ifndef TEXTURE_H
#define TEXTURE_H

#include <GL/glew.h>
#include <string>

class Texture
{
public:
    Texture();
    ~Texture();

    bool loadTexture(std::string file);

    void useTexture(GLenum location);

private:
    GLuint m_textureID;
    int m_width;
    int m_height;
    int m_bitDepth;
};

#endif // TEXTURE_H
