#ifndef SERIALCONNECTION_H
#define SERIALCONNECTION_H

#include <QByteArray>
#include <QObject>
#include <QSerialPort>
#include <QTextStream>
#include <QTimer>

// qt
#include <QPointer>

class SerialConnection : public QObject
{
  Q_OBJECT
public:
    SerialConnection(QObject* parent = nullptr);
    ~SerialConnection();

    void write(const QByteArray &writeData);
    void closeConnection();

public slots:
    void SetUpConnection(
            QString in_port_name = "COM5",
            qint32  in_baud      = QSerialPort::Baud9600,
            QSerialPort::DataBits    in_data_bits    = QSerialPort::DataBits::Data8,
            QSerialPort::StopBits    in_stop_bits    = QSerialPort::StopBits::OneStop,
            QSerialPort::Parity      in_parity       = QSerialPort::Parity::NoParity,
            QSerialPort::FlowControl in_flow_control = QSerialPort::FlowControl::NoFlowControl
            );


//signals:
//    void finished();
//    void error(QString err);

private:
    QPointer<QSerialPort> serial_port;

//    QPointer<QThread> thread;

    // connection settings
    QString                  port_name;
    qint32                   baud;
    QSerialPort::DataBits    data_bits;
    QSerialPort::StopBits    stop_bits;
    QSerialPort::Parity      parity;
    QSerialPort::FlowControl flow_control;
};



#endif // SERIALCONNECTION_H
