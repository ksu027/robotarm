#include "robotarm.h"

#include <scene/sceneobjects/gmpathtrack.h>
#include <memory>

#include "link.h"
#include "joint.h"

#include <math.h>

#include <QDebug>

using vec3 = GMlib::Vector<float, 3>;

RobotArm::RobotArm(std::shared_ptr<GMlib::Scene> scene)
    : m_scene(scene), m_angle(0), m_handSpace(0)
{

}

RobotArm::~RobotArm()
{
//    std::cout << "Virtual arm deleted" << std::endl;
}

void RobotArm::setup()
{
    // Arm dimension varaibles
    baseRadius = 5.0f;
    baseHeight = 4.5f;
    linkRadius = 1.0f;
    jointRadius = 1.3f;
    link1Length = 15.5f-/*2.0f**/jointRadius;
    link2Length = 20.0f-/*2.0f**/jointRadius;
    link3Length = 6.5f -/*2.0f**/jointRadius;
    pencilLength = 16.0f;
    pencilRadius = 0.45f;

    gap_base_edge_join1 = 3.0f;//offset from base's edge to the center of the joint 1. look from top
    gap_base_height_join1 = 3.0f;//offset from base's top to the center of the joint 1. look from side


    int joint1Angle = 90;
    int joint2Angle = 90;
    int joint3Angle = 90;

    // Materials
    GMlib::Material chrome(GMlib::GMmaterial::chrome());
    chrome.set(45.0);

    GMlib::Material red(GMlib::GMmaterial::ruby());

    GMlib::Material black(GMlib::GMmaterial::blackPlastic());
    black.set(45.0);

    //Objects
    m_base_joint = std::make_shared<Joint>(0.1f, vec3(0, 0, 1));
    m_base_joint->rotate(GMlib::Angle(90),vec3(1,0,0));
    m_base_joint->toggleDefaultVisualizer();
    m_base_joint->sample(32, 32, 1, 1);
    m_base_joint->setMaterial(black);
    m_scene->insert(m_base_joint.get());

    auto base = new Link(baseRadius, baseHeight);
    base->toggleDefaultVisualizer();
    base->sample(32, 32, 1, 1);
    base->setMaterial(black);
    m_base_joint->insert(base);

    auto joint1 = std::make_shared<Joint>(jointRadius, vec3(1, 0, 0));
    //joint1->translate(vec3(0.0f, (baseRadius - jointRadius), -baseHeight / 2));
    joint1->translate(vec3(0.0f, (baseRadius - gap_base_edge_join1), -baseHeight / 2 - gap_base_height_join1));
    joint1->rotate(GMlib::Angle(90),vec3(1,0,0));
    joint1->rotate(GMlib::Angle(180),vec3(0,1,0));
    joint1->setAngle(joint1Angle);
    joint1->toggleDefaultVisualizer();
    joint1->sample(32, 32, 1, 1);
    joint1->setMaterial(black);
    m_joints.push_back(std::move(joint1));
    base->insert(m_joints.back().get());

    auto link1 = std::make_shared<Link>(linkRadius, link1Length);
    link1->translate(vec3(0.0f, 0.0f, link1Length / 2 + jointRadius / 2));
    link1->toggleDefaultVisualizer();
    link1->sample(32, 32, 1, 1);
    link1->setMaterial(chrome);
    m_links.push_back(std::move(link1));
    m_joints.back()->insert(m_links.back().get());

    auto joint2 = std::make_shared<Joint>(jointRadius, vec3(1, 0, 0));
    joint2->translate(vec3(0.0f, 0.0f, jointRadius / 2 + link1Length / 2));
    joint2->rotate(GMlib::Angle(180),vec3(0,0,1));
    joint2->setAngle(joint2Angle);
    joint2->toggleDefaultVisualizer();
    joint2->sample(32, 32, 1, 1);
    joint2->setMaterial(black);
    m_joints.push_back(std::move(joint2));
    m_links.back()->insert(m_joints.back().get());

    auto link2 = std::make_shared<Link>(linkRadius, link2Length);
    link2->translate(vec3(0.0f, 0.0f, link2Length / 2 + jointRadius / 2));
    link2->toggleDefaultVisualizer();
    link2->sample(32, 32, 1, 1);
    link2->setMaterial(chrome);
    m_links.push_back(std::move(link2));
    m_joints.back()->insert(m_links.back().get());

    auto joint3 = std::make_shared<Joint>(jointRadius, vec3(1, 0, 0));
    joint3->translate(vec3(0.0f, 0.0f, jointRadius / 2 + link2Length / 2));
    joint3->rotate(GMlib::Angle(-90),vec3(1,0,0));
    joint3->rotate(GMlib::Angle(180),vec3(0,1,0));
    joint3->setAngle(joint3Angle);
    joint3->toggleDefaultVisualizer();
    joint3->sample(32, 32, 1, 1);
    joint3->setMaterial(black);
    m_joints.push_back(std::move(joint3));
    m_links.back()->insert(m_joints.back().get());

    auto link3 = std::make_shared<Link>(linkRadius, link3Length);
    link3->translate(vec3(0.0f, 0.0f, link3Length / 2 + jointRadius / 2));
    link3->toggleDefaultVisualizer();
    link3->sample(32, 32, 1, 1);
    link3->setMaterial(chrome);
    m_links.push_back(std::move(link3));
    m_joints.back()->insert(m_links.back().get());

    m_end = std::make_shared<Joint>(jointRadius, vec3(1, 0, 0));
    m_end->translate(vec3(0.0f, 0.0f, jointRadius / 2 + link3Length / 2));
    //end->setAngle(90);
    m_end->toggleDefaultVisualizer();
    m_end->sample(32, 32, 1, 1);
    m_end->setMaterial(black);
    m_links.back()->insert(m_end.get());

    m_finger1 = std::make_shared<Link>(0.75f, 3.0f);
    m_finger1->translate(vec3(1.5f, 0.0f, 1.5f));
    m_finger1->toggleDefaultVisualizer();
    m_finger1->sample(32, 32, 1, 1);
    m_finger1->setMaterial(black);
    m_end->insert(m_finger1.get());

    m_finger2 = std::make_shared<Link>(0.75f, 3.0f);
    m_finger2->translate(vec3(-1.5f, 0.0f, 1.5f));
    m_finger2->toggleDefaultVisualizer();
    m_finger2->sample(32, 32, 1, 1);
    m_finger2->setMaterial(black);
    m_end->insert(m_finger2.get());

    m_handSpace = 1.5f;

    auto pencil = new Link(pencilRadius, pencilLength);
    pencil->translate(vec3(0, 0, 1));
    pencil->rotate(GMlib::Angle(90), vec3(1, 0, 0));
    pencil->toggleDefaultVisualizer();
    pencil->sample(32, 32, 1, 1);
    pencil->setMaterial(red);
    m_end->insert(pencil);

    m_pencilTip = std::make_shared<Joint>(pencilRadius, vec3(0, 0, 1));
    m_pencilTip->translate(vec3(0.0f, 0.0f, -(pencilLength / 2 + pencilRadius / 2)));
    m_pencilTip->toggleDefaultVisualizer();
    m_pencilTip->sample(32, 32, 1, 1);
    m_pencilTip->setMaterial(black);
    pencil->insert(m_pencilTip.get());
    auto ptrack = new GMlib::PathTrack();
    ptrack->setLineWidth(2);
    m_pencilTip->insert(ptrack);


    //test joints
    GMlib::Material bronze(GMlib::GMmaterial::bronze());
    bronze.set(45.0);
    for (size_t i=0;i<=m_joints.size()+2;i++)
    {
        test_joints.push_back(std::make_shared<Joint>(1.2, vec3(1, 0, 0)));
        test_joints.back()->sample(32, 32, 1, 1);
        test_joints.back()->toggleDefaultVisualizer();
        test_joints.back()->setMaterial(bronze);
        m_scene->insert(test_joints.back().get());
    }

}

void RobotArm::remove(std::shared_ptr<GMlib::Scene> scene)
{
}

GMlib::Vector<float, 3> RobotArm::lastJoint() const
{
    //returns global coordinates of the last joint gmlib

    //return m_links.back()->getChildren().back()->getGlobalPos();
    return m_joints.back()->getGlobalPos();

}

GMlib::Vector<float, 3> RobotArm::betweenFingers() const
{
    //returns global coordinates of the center between fingers gmlib
    if (m_links.back()->getChildren().back()->getChildren().size()>0)
    {
        auto finger_1_pos = m_links.back()->getChildren().back()->getChildren()[0]->getGlobalPos();
        auto finger_2_pos = m_links.back()->getChildren().back()->getChildren()[1]->getGlobalPos();
        return finger_1_pos-(finger_1_pos-finger_2_pos)/2.0;
    }
    else {
        qDebug()<<"No fingers";
        return GMlib::Vector<float,3>();
    }
}

GMlib::HqMatrix<float,3> RobotArm::lastJointDK() const
{
    //returns global coordinates of the last joint Direct Kinematics

    return m_base_joint->getMatrix()*m_joints[0]->getMatrix()*
             m_links[0]->getMatrix()*m_joints[1]->getMatrix()*
             m_links[1]->getMatrix()*m_joints[2]->getMatrix()*
             m_links[2]->getMatrix()*m_links[2]->getChildren()[0]->getMatrix();




}

GMlib::HqMatrix<float,3> RobotArm::betweenFingersDK() const
{
    //returns global coordinates of the center between fingers
    if (m_links.back()->getChildren().back()->getChildren().size()>0)
    {
        auto finger_1_mat = m_base_joint->getMatrix()*m_joints[0]->getMatrix()*m_links[0]->getMatrix()*
                            m_joints[1]->getMatrix()*m_links[1]->getMatrix()*
                            m_joints[2]->getMatrix()*m_links[2]->getMatrix()*
                            m_links[2]->getChildren()[0]->getMatrix()*m_links[2]->getChildren()[0]->getChildren()[0]->getMatrix();
        auto finger_2_mat = m_base_joint->getMatrix()*m_joints[0]->getMatrix()*m_links[0]->getMatrix()*
                            m_joints[1]->getMatrix()*m_links[1]->getMatrix()*
                            m_joints[2]->getMatrix()*m_links[2]->getMatrix()*
                            m_links[2]->getChildren()[0]->getMatrix()*m_links[2]->getChildren()[0]->getChildren()[1]->getMatrix();


//        auto test_matrix = finger_1_mat;
//        qDebug()<<"rotation finger 1";
//        qDebug()<<test_matrix.getRow(0)[0]<<" "<<test_matrix.getRow(0)[1]<<" "<<test_matrix.getRow(0)[2]<<" "<<test_matrix.getRow(0)[3];
//        qDebug()<<test_matrix.getRow(1)[0]<<" "<<test_matrix.getRow(1)[1]<<" "<<test_matrix.getRow(1)[2]<<" "<<test_matrix.getRow(1)[3];
//        qDebug()<<test_matrix.getRow(2)[0]<<" "<<test_matrix.getRow(2)[1]<<" "<<test_matrix.getRow(2)[2]<<" "<<test_matrix.getRow(2)[3];
//        qDebug()<<test_matrix.getRow(3)[0]<<" "<<test_matrix.getRow(3)[1]<<" "<<test_matrix.getRow(3)[2]<<" "<<test_matrix.getRow(3)[3];

//        test_matrix = finger_2_mat;
//        qDebug()<<"rotation finger 2";
//        qDebug()<<test_matrix.getRow(0)[0]<<" "<<test_matrix.getRow(0)[1]<<" "<<test_matrix.getRow(0)[2]<<" "<<test_matrix.getRow(0)[3];
//        qDebug()<<test_matrix.getRow(1)[0]<<" "<<test_matrix.getRow(1)[1]<<" "<<test_matrix.getRow(1)[2]<<" "<<test_matrix.getRow(1)[3];
//        qDebug()<<test_matrix.getRow(2)[0]<<" "<<test_matrix.getRow(2)[1]<<" "<<test_matrix.getRow(2)[2]<<" "<<test_matrix.getRow(2)[3];
//        qDebug()<<test_matrix.getRow(3)[0]<<" "<<test_matrix.getRow(3)[1]<<" "<<test_matrix.getRow(3)[2]<<" "<<test_matrix.getRow(3)[3];

        auto col_1 = finger_1_mat.getCol(3);
        auto col_2 = finger_2_mat.getCol(3);
        auto new_col = col_1-(col_1-col_2)/2.0;

        finger_1_mat.setCol(new_col,3);
        return finger_1_mat;

    }
    else {
        qDebug()<<"No fingers";
        return GMlib::Vector<float,3>();
    }

}

void RobotArm::getJointsPoses(std::vector<GMlib::Angle> & in_angles, std::vector<GMlib::Vector<float,3>> & out_poses)
{
    //returns global poses of joints with input angles to output vector of poses
    //create local copy of joints, so that real ones will not be affected

    std::vector<Joint> copy_joints;
    copy_joints.push_back(*m_base_joint);
    for (auto & joint:m_joints)
        copy_joints.push_back(*joint);
    copy_joints.push_back(*m_end);

    std::vector<Link> copy_links;
    for (auto & link:m_links)
        copy_links.push_back(*link);

    if (out_poses.size()>0) out_poses.clear();

    GMlib::HqMatrix<float,3> new_pose = copy_joints[0].getMatrix();
    copy_joints[0].setMatrix(new_pose);
    copy_joints[0].pseudoSimulate(in_angles[0]);
    out_poses.push_back(copy_joints[0].getMatrix().getCol(3));
    test_joints[0]->setMatrix(out_poses.back());

    new_pose = copy_joints[0].getMatrix()*copy_joints[1].getMatrix();
    copy_joints[1].setMatrix(new_pose);
    copy_joints[1].pseudoSimulate(in_angles[1]);
    out_poses.push_back(copy_joints[1].getMatrix().getCol(3));
    test_joints[1]->setMatrix(out_poses.back());

    new_pose = copy_joints[1].getMatrix()*copy_links[0].getMatrix()*copy_joints[2].getMatrix();
    copy_joints[2].setMatrix(new_pose);
    copy_joints[2].pseudoSimulate(in_angles[2]);
    out_poses.push_back(copy_joints[2].getMatrix().getCol(3));
    test_joints[2]->setMatrix(out_poses.back());

    new_pose = copy_joints[2].getMatrix()*copy_links[1].getMatrix()*copy_joints[3].getMatrix();
    copy_joints[3].setMatrix(new_pose);
    copy_joints[3].pseudoSimulate(in_angles[3]);
    out_poses.push_back(copy_joints[3].getMatrix().getCol(3));
    test_joints[3]->setMatrix(out_poses.back());

    new_pose = copy_joints[3].getMatrix()*copy_links[2].getMatrix()*copy_joints[4].getMatrix();
    copy_joints[4].setMatrix(new_pose);
    //copy_joints[4].pseudoSimulate(in_angles[4]); //as there is no rotation for that joint
    out_poses.push_back(copy_joints[4].getMatrix().getCol(3));
    test_joints[4]->setMatrix(out_poses.back());

    return;
}

void RobotArm::translateJointsInVector(std::vector<Joint> &copy_joints, size_t from_pos)
{
    GMlib::Point<float,3> new_pos;
    for (auto j=from_pos+1;j<copy_joints.size();j++)
    {
        if (j==1)
            new_pos = (copy_joints[j-1].getMatrixGlobal()*copy_joints[j].getMatrix()).getCol(3);
        else
            new_pos = (copy_joints[j-1].getMatrixGlobal()*m_links[j-2]->getMatrix()*copy_joints[j].getMatrix()).getCol(3);
        copy_joints[j].translateGlobal(new_pos-copy_joints[j].getGlobalPos());
        //call the same part for others recursivelly
        //translateJointsInVector(copy_joints,j);
    }
}

void RobotArm::moveArmToPoint(GMlib::Point<float, 3> in_point)
{
    auto base_point = m_base_joint->getGlobalPos();

    qDebug()<<"move to: "<<in_point[0]<<" "<<in_point[1]<<" "<<in_point[2]<<" ";
    qDebug()<<"base is: "<<base_point[0]<<" "<<base_point[1]<<" "<<base_point[2]<<" ";

    //point[0] - x
    //point[1] - z or green axis
    //point[2] - y
    //find gamma
    auto L1x = -(in_point[0]-base_point[0]); // as it is inversed on the screen
    auto L1y = in_point[2]-base_point[2];
    auto gamma_rad = atan2(L1x,L1y);

    std::vector<GMlib::Vector<float,3>> pseudo_poses;
    std::vector<GMlib::Angle> current_angles {gamma_rad,m_joints[0]->angle(),m_joints[1]->angle(),m_joints[2]->angle()/*,m_end->angle()*/};
    this->getJointsPoses(current_angles,pseudo_poses);

    GMlib::Point<float,3> joint1_pos = pseudo_poses[1]; //joint1 position after the virtual turn of the base to the desired angle

    GMlib::Point<float,3> in_point_proj = in_point;                 //projection of desired pos on XY on the z level of joint1
    in_point_proj[1]=joint1_pos[1];
    qDebug()<<"joint2 "<<joint1_pos[0]<<" "<<joint1_pos[1]<<" "<<joint1_pos[2];
    auto S  = link1Length+jointRadius;          //from joint2 to joint1
    qDebug()<<"S = "<<S;
    auto H  = link2Length+jointRadius;          //from joint3 to joint2
    qDebug()<<"H = "<<H;
    auto L  = (in_point-joint1_pos).getLength();          //from joint3 to joint1
    qDebug()<<"L = "<<L;
    auto L1 = (in_point_proj-joint1_pos).getLength();     //from projection of desired pos to joint1
    qDebug()<<"L1 = "<<L1;
    auto z0 = in_point[1]-joint1_pos[1];
    qDebug()<<"z0 = "<<z0;

    auto alpha  = acos(z0/L)+acos(-(S*S-H*H-L*L)/(2.0f*H*L));   //for joint "end"
    qDebug()<<"alpha = "<<alpha*180.0/M_PI;
    auto betta  = acos((S*S+H*H-L*L)/(2.0*S*H));                 //for joint 3
    qDebug()<<"betta = "<<betta*180.0/M_PI;
    auto alpha2 = acos((H*H+L*L-S*S)/(2.0*H*L));
    qDebug()<<"alpha2 = "<<alpha2*180.0/M_PI;
    auto alpha1 = alpha - alpha2; //alpha1 = acos(L1/L);
    qDebug()<<"alpha1 = "<<alpha1*180.0/M_PI;
    auto alpha11 = acos(L1/L);
    qDebug()<<"alpha11 = "<<alpha11*180.0/M_PI;

    auto tetta1  = M_PI - alpha2 - betta;
    auto tetta2  = M_PI/2.0  - alpha1;
    auto tetta   = tetta1+tetta2; //for joint2

    qDebug()<<"tetta: "<<tetta * 180.0 / M_PI;

    current_angles = {gamma_rad,tetta,M_PI-betta,m_joints[2]->angle()/*,m_end->angle()*/};
    this->getJointsPoses(current_angles,pseudo_poses);

    this->setBaseAngle(gamma_rad);
    this->setShoulderAngle(tetta);
    this->setElbowAngle(M_PI-betta);

    GMlib::HqMatrix<float,3> test_matrix = m_joints[0]->getMatrix();
    QString str_print;
    qDebug()<<"full";
    for (auto i = 0; i<4;i++)
    {
        str_print = "";
        for (auto j = 0; j<4; j++)
        {
            str_print = str_print + QString::number(test_matrix[i][j]) + "  ";
        }
        qDebug()<<str_print;
    }

    auto rot_mat = test_matrix.getRotationMatrix();
    qDebug()<<"rot";
    for (auto i = 0; i<3;i++)
    {
        str_print = "";
        for (auto j = 0; j<3; j++)
        {
            str_print = str_print + QString::number(rot_mat[i][j]) + "  ";
        }
        qDebug()<<str_print;
    }


}

void RobotArm::setJointAngle(size_t index, int angle)
{
    m_joints[index]->setAngle(angle);
}

int RobotArm::jointAngle(size_t index) const
{
    return m_joints[index]->angle();
}

void RobotArm::setBaseAngle(GMlib::Angle angle)
{
    m_base_joint->setAngle(angle);
}

int RobotArm::baseAngle() const
{
    return m_angle;
}

void RobotArm::setShoulderAngle(GMlib::Angle angle) {
    setJointAngle(0, angle);
}

int RobotArm::shoulderAngle() const
{
    return jointAngle(0);
}

void RobotArm::setElbowAngle(GMlib::Angle angle)
{
    setJointAngle(1, angle);
}

int RobotArm::elbowAngle() const
{
    return jointAngle(1);
}

void RobotArm::setWristAngle(GMlib::Angle angle)
{
    setJointAngle(2, angle);
}

int RobotArm::wristAngle() const
{
    return jointAngle(2);
}

void RobotArm::setGripWidth(int space)
{
    float fSpace = space / 10.0f;
    float diff = (fSpace - m_handSpace) / 2.0f;
    m_finger1->translate(vec3(diff, 0.0f, 0.0f));
    m_finger2->translate(vec3(-diff, 0.0f, 0.0f));
    m_handSpace = fSpace;
}

float RobotArm::handSpace() const
{
    return m_handSpace;
}

void RobotArm::setSpeed(int speed)
{
    m_base_joint->setSpeed(speed);
    for(size_t i = 0; i < m_joints.size(); i++) {
        m_joints[i]->setSpeed(speed);
    }
}

double RobotArm::speed() const
{
    return m_base_joint->speed();
}

void RobotArm::copyObjRotate(GMlib::SceneObject & in_obj, GMlib::Angle angle, GMlib::Angle target_angle, GMlib::Angle current_angle, GMlib::Vector<float, 3> rotate_axel)
{
    double x = target_angle.getRad();
    double y = current_angle.getRad();
    double diff = std::atan2(std::sin(x-y), std::cos(x-y)); // https://stackoverflow.com/questions/1878907/the-smallest-difference-between-2-angles
    if(std::abs(diff) > 0.0001) {
        in_obj.rotate(GMlib::Angle(diff), rotate_axel);
    }
}

void RobotArm::JacobianIK(std::vector<GMlib::Angle> & input_angles)
{
//    auto max_iter = 1000;
//    auto epsilon = 0.1;
//    auto distance = epsilon + 1.0;
//    auto iter = 0;
//    auto step = 0.01;


//    std::vector<GMlib::Vector<float,3>> cur_joint_poses; // add current joint poses
//    GMlib::Vector<float,3> goal_pos;
//    GMlib::Vector<float,3> cur_eef_pos;

//    while ( distance > epsilon and iter<=max_iter)
//    {
//        auto delta_angles = getDeltaAngles(goal_pos,cur_eef_pos,cur_joint_poses);
//        for (size_t i = 0; i<input_angles.size(); i++) {
//            input_angles[i] += delta_angles[i]*step;
//        }
//        //here
//        //pseudosimulate and get new cur_joint_poses to changed "input_angles" with getJointsPoses
//        //cur_eef =calculate disstance between ( pseudomodel's eef coord and goal coord)
//        iter++;
//    }
}

GMlib::Vector<float,3> RobotArm::getDeltaAngles(GMlib::Vector<float,3> & target_pos, GMlib::Vector<float,3> & cur_eef_pos, std::vector<GMlib::Vector<float,3>> & cur_joint_poses)
{
//    auto Jt = getJacobianTranspose(cur_eef_pos, cur_joint_poses);
//    auto V = target_pos - cur_eef_pos;
//    return Jt * V; //these are the delta angles
    GMlib::Vector<float,3> ret;
    return ret;
}

GMlib::Matrix<float, 3, 3> RobotArm::getJacobianTranspose(GMlib::Vector<float,3> & cur_eef_pos, std::vector<GMlib::Vector<float,3>> & cur_joint_poses)
{
    //we do it for 3 joints only as we assume that base joint has already rotate to the angle which is in one plane with the goal coordinates
//    GMlib::Vector<float,3> J_A = m_joints[1]->getAxel() ^ (cur_eef_pos - cur_joint_poses[1]);
//    GMlib::Vector<float,3> J_B = m_joints[2]->getAxel() ^ (cur_eef_pos - cur_joint_poses[2]);
//    GMlib::Vector<float,3> J_C = m_joints[3]->getAxel() ^ (cur_eef_pos - cur_joint_poses[3]);

//    GMlib::Matrix<float, 3, 3> J;
//    J.setCol(J_A,0);
//    J.setCol(J_B,1);
//    J.setCol(J_C,2);
//    J.setTransposed(J);
//    return J;
    GMlib::Matrix<float, 3, 3> ret;
    return ret;
}
