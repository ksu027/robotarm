#include "mesh.h"

#include "meshvisualizer.h"
#include <scene/sceneobjects/gmsphere3d.h>

Mesh::Mesh()
    : SceneObject(), m_isExecuting(false), m_speed(1),
      m_currAngle(0), m_targetAngle(0), m_minAngle(0), m_maxAngle(180),
      m_rotAxel(GMlib::Vector<float, 3>(0, 0, 1)),
      m_currPos(GMlib::Vector<float, 3>(0, 0, 0)),
      m_targetPos(GMlib::Vector<float, 3>(0, 0, 0))
{
    setSurroundingSphere(new GMlib::Sphere3D(5.0f));

    m_default_visualizer = new MeshVisualizer();
    insertVisualizer(m_default_visualizer);

    _material.setDif(GMlib::Color(255, 255, 255, 255));
    _material.setAmb(GMlib::Color(20, 20, 20, 255));
    _material.setSpc(GMlib::Color(255, 255, 255, 255));
}

Mesh::Mesh(const Mesh &copy) : GMlib::SceneObject(copy)
{
    // Copy
}

Mesh::~Mesh()
{
    // Delete
}

void Mesh::createMesh(std::string name, std::vector<float>& vs, std::vector<unsigned int>& is)
{
    m_name = name;
    m_default_visualizer->setupGeometry(vs, is);
}

void Mesh::setAngle(GMlib::Angle angle, bool animate)
{
    m_targetAngle = angle;
    clampTargetAngle();

    if(!animate)
    {        
        rotate(getAngleDiff(m_currAngle, m_targetAngle), m_rotAxel);
        m_currAngle = angle;
    }
    else {
        m_isExecuting = true;
    }
}

void Mesh::setAngleDelta(GMlib::Angle angleDelta, bool animate)
{
    setAngle(m_currAngle + angleDelta, animate);
}

void Mesh::setPos(GMlib::Vector<float, 3> pos, bool animate)
{
    m_targetPos = pos;

    if(!animate)
    {
        GMlib::Vector<float, 3> diffPos = m_targetPos - m_currPos;
        translate(diffPos);
        m_currPos = pos;
    }
    else {
        m_isExecuting = true;
    }
}

void Mesh::setSpeed(double speed)
{
    m_speed = speed / 600;
}

void Mesh::setRotAxel(GMlib::Vector<float, 3> rotAxel)
{
    m_rotAxel = rotAxel;
}

void Mesh::setTexture(Texture* tex)
{
    m_default_visualizer->setTexture(tex);
}

GMlib::Angle Mesh::getAngleDiff(GMlib::Angle current_angle, GMlib::Angle desired_angle)
{
    // TODO: Use a better way of finding difference of angles.
//    double x,y,diff_angle;
//    x = desired_angle.getRad();
//    y = current_angle.getRad();
//    diff_angle = std::atan2(std::sin(x-y), std::cos(x-y));
//    return diff_angle;
    return GMlib::Angle(desired_angle - current_angle);
}

void Mesh::localSimulate(double dt)
{
    if(m_isExecuting)
    {
        bool doneExecuting = true;

        // Simulate rotation
        double x = m_targetAngle.getRad();
        double y = m_currAngle.getRad();
        double diff = std::atan2(std::sin(x-y), std::cos(x-y)); // https://stackoverflow.com/questions/1878907/the-smallest-difference-between-2-angles
        if(std::abs(diff) > 0.0001) {
            double da = m_speed * dt * ((diff > 0) - (diff < 0));
            if(std::abs(diff) < std::abs(da)) da = diff;
            m_currAngle += da;
            rotate(GMlib::Angle(da), m_rotAxel);
            doneExecuting = false;
        }

        // Simulate movement
        GMlib::Vector<float, 3> diffPos = m_targetPos - m_currPos;
        auto diffLength = diffPos.getLength();
        if(diffLength > 0.0001f)
        {
            float length = m_speed * dt * 10.0f;
            if(diffLength > length)
            {
                diffPos.setLength(length);
            }
            translate(diffPos);
            m_currPos += diffPos;
            doneExecuting = false;
        }

        m_isExecuting = !doneExecuting;
    }
}

// Makes sure the target angle is within the allowed angles.
void Mesh::clampTargetAngle()
{
    if(m_targetAngle.getRad() < m_minAngle.getRad())
    {
        m_targetAngle = GMlib::Angle(m_minAngle.getRad());
    }
    else if(m_targetAngle.getRad() > m_maxAngle.getRad())
    {
        m_targetAngle = GMlib::Angle(m_maxAngle.getRad());
    }
}
