#include "serialconnection.h"

// qt
#include <QThread>
#include <QtDebug>

SerialConnection::SerialConnection(QObject* parent)
    : QObject(parent), serial_port{new QSerialPort()}/*, thread{new QThread}*/
{

}

SerialConnection::~SerialConnection()
{
//    serial_port->deleteLater();
//    thread->deleteLater();
}

void SerialConnection::SetUpConnection(QString in_port_name, qint32 in_baud, QSerialPort::DataBits in_data_bits, QSerialPort::StopBits in_stop_bits, QSerialPort::Parity in_parity, QSerialPort::FlowControl in_flow_control)
{
    if (serial_port->isOpen())
        serial_port->close();

    serial_port->setPortName(in_port_name);
    serial_port->setBaudRate(in_baud);
    serial_port->setDataBits(in_data_bits);
    serial_port->setStopBits(in_stop_bits);
    serial_port->setParity(in_parity);
    serial_port->setFlowControl(in_flow_control);

    if (serial_port->open(QIODevice::ReadWrite))
        qInfo()<<"Port "<<in_port_name<<" is opened successfully";
    else
        qInfo()<<"Port "<<in_port_name<<" is not opened: "<<serial_port->errorString();
}

void SerialConnection::write(const QByteArray &writeData)
{
    if (serial_port->isOpen())
    {
        serial_port->write(writeData);
        serial_port->waitForReadyRead(10);
        serial_port->flush();

    } else
        qInfo()<<"Can't write to port "<<serial_port->portName()<<", as it is not opened: "<<serial_port->errorString();
}


void SerialConnection::closeConnection()
{
    if (serial_port->isOpen())
        serial_port->close();
}
