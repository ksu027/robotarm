#ifndef MODELLOADER_H
#define MODELLOADER_H

#include <vector>
#include <unordered_map>
#include <string>

#include "mesh.h"
#include "texture.h"

struct Model {
    std::unordered_map<std::string, Mesh*> meshes;
    std::vector<Texture*> textures;
};

class ModelLoader
{
public:
    static Model LoadModelObj(const std::string& baseDir, const std::string& fileName);
};

#endif // MODELLOADER_H
