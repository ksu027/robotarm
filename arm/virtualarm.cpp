#define TINY_EXPR_IMPLEMENTATION

#include "virtualarm.h"

#include <core/utils/gmutils.h>
#include <scene/sceneobjects/gmsphere3d.h>

#include "gripper.h"
#include "joint.h" //for testing

VirtualArm::VirtualArm()
    : GMlib::SceneObject(), m_arm_controller_ptr(nullptr), m_env(nullptr), m_base(nullptr),
      m_shoulder(nullptr), m_elbow(nullptr), m_wrist(nullptr), m_gripper(nullptr), m_gripperTrack(nullptr)
{
}

VirtualArm::VirtualArm(ArmController *in_ac)
    : GMlib::SceneObject(), m_arm_controller_ptr(in_ac), m_env(nullptr), m_base(nullptr),
      m_shoulder(nullptr), m_elbow(nullptr), m_wrist(nullptr), m_gripper(nullptr), m_gripperTrack(nullptr)
{
}

VirtualArm::VirtualArm(const VirtualArm &copy) : GMlib::SceneObject(copy)
{
    // Copy
}

VirtualArm::~VirtualArm()
{
    // Delete
    cleanUp();
}

void VirtualArm::localSimulate(double dt)
{
    m_isExecuting = m_base->isExecuting() || m_shoulder->isExecuting()
            || m_elbow->isExecuting() || m_wrist->isExecuting();

    if(!m_actionQueue.empty())
    {
        if(!m_isExecuting)
        {
            doAction(m_actionQueue.front());
            m_arm_controller_ptr->updateGUISliders(m_actionQueue.front().baseAngle,m_actionQueue.front().shoulderAngle,m_actionQueue.front().elbowAngle,m_actionQueue.front().wristAngle);
            m_actionQueue.pop();
            m_isExecuting = true;
        }
    }

    //for plotting
    if(m_isExecuting) {
        m_chartUpdateElapsed += dt;
        if(m_chartUpdateElapsed > m_chartUpdateRate) {
            auto eefPos = getEEF();
            m_arm_controller_ptr->eefPosChanged(QVariant(eefPos[0]), QVariant(eefPos[1]), QVariant(eefPos[2]));
            m_chartUpdateElapsed = 0.0;
        }
    }
}

void VirtualArm::setup(std::shared_ptr<GMlib::Scene> scene)
{
    setSurroundingSphere(new GMlib::Sphere3D());

//    Model model = ModelLoader::LoadModel("../../robotarm/models/AL5D.obj"); // More detailed model
    Model model = ModelLoader::LoadModelObj("../../robotarm/models/", "AL5D.obj"); // Detailed model with tiny_obj_loader
//    Model model = ModelLoader::LoadModel("../../robotarm/models/AL5D_Simple.obj"); // Simple model for quicker loading
    m_meshes = model.meshes;
    m_textures = model.textures;

    //this->rotate(GMlib::Angle(90), GMlib::Vector<float, 3>(1, 0, 0));
    //this->rotate(GMlib::Angle(90), GMlib::Vector<float, 3>(0, 1, 0));
    scene->insert(this);

    m_env = m_meshes.find("Environment")->second;
    //m_env->rotate(GMlib::Angle(-90), GMlib::Vector<float, 3>(0, 1, 0));
    this->insert(m_env);

    //when scene is not rotated
    //     env   is not rotated
    //     base  is rotated 90" and rot axel = -1
    //then we have rotational degrees ox Z axis of base and scene consistent

    m_base = m_meshes.find("Base")->second;
    m_base->rotate(GMlib::Angle(90), GMlib::Vector<float, 3>(0, 1, 0));
    m_base->setRotAxel(GMlib::Vector<float, 3>(0, -1, 0));
    m_base->setAngle(90, false);
    m_base->setAngleLimits(0, 165);
    m_env->insert(m_base);

    m_shoulder = m_meshes.find("Shoulder")->second;
    m_shoulder->translate(GMlib::Vector<float, 3>(11.968f, 71.347f, 0.0f));
    m_shoulder->rotate(GMlib::Angle(-90), GMlib::Vector<float, 3>(0, 0, 1));
    m_shoulder->setAngle(90, false);
    m_shoulder->setAngleLimits(0, 180);
    m_base->insert(m_shoulder);

    m_elbow = m_meshes.find("Elbow")->second;
    m_elbow->translate(GMlib::Vector<float, 3>(0.028f, 146.293f, 0.0f));
    m_elbow->rotate(GMlib::Angle(90), GMlib::Vector<float, 3>(0, 0, 1));
    m_elbow->setRotAxel(GMlib::Vector<float, 3>(0, 0, -1));
    m_elbow->setAngle(90, false);
    m_elbow->setAngleLimits(0, 165);
    m_shoulder->insert(m_elbow);

    m_wrist = m_meshes.find("Wrist")->second;
    m_wrist->translate(GMlib::Vector<float, 3>(180.654f, 4.43f, 0.0f));
    m_wrist->rotate(GMlib::Angle(-90), GMlib::Vector<float, 3>(0, 0, 1));
    m_wrist->setAngle(90, false);
    m_wrist->setAngleLimits(0, 180);
    m_elbow->insert(m_wrist);

    m_gripper = new Gripper(m_meshes, m_wrist);

    m_gripperTrack = new GMlib::PathTrack(200, 5, GMlib::GMcolor::red());
    m_gripperTrack->setVisible(false);
//    m_gripperTrack->setLineWidth(2.0f);
    m_gripperTrack->translate(GMlib::Vector<float, 3>(72, 0, 4.583f));
    m_wrist->insert(m_gripperTrack);

    //test joints
    GMlib::Material bronze(GMlib::GMmaterial::bronze());
    bronze.set(45.0);
    for (size_t i=0;i<=5;i++)
    {
        m_test_joints.push_back(std::make_shared<Joint>(20.0, GMlib::Vector<float, 3>(1, 0, 0)));
        m_test_joints.back()->sample(32, 32, 1, 1);
        m_test_joints.back()->toggleDefaultVisualizer();
        m_test_joints.back()->setMaterial(bronze);
        scene->insert(m_test_joints.back().get());
    }
}

void VirtualArm::cleanUp()
{
    // Delete test_joints
    for(size_t i = 0; i < m_test_joints.size(); i++)
    {
        if(m_test_joints[i]) {
            _scene->remove(m_test_joints[i].get());
            m_test_joints[i].reset();
        }
    }
}

void VirtualArm::executeActions(std::queue<ArmAction> actions)
{
    std::cout << "Executing " << actions.size() << " actions!" << std::endl;
    m_actionQueue = actions;
}

void VirtualArm::executeCoords(std::vector<GMlib::Vector<float, 3> > coords)
{
    if(m_isExecuting) {
        std::cout << "Wait for previous action to finish before doing something new" << std::endl;
        return;
    }
    auto start = std::chrono::high_resolution_clock::now();

    auto step_length = 5.0f;
    int steps;
    std::queue<ArmAction> actions;
    GMlib::Vector<float,3> vector_between;

    actions.push(getActionForPoint(coords[0]));
    for( size_t i=1; i<coords.size(); i++)
    {
        //add aditional points to make better picture
        vector_between = coords[i]-coords[i-1];
        steps = int(vector_between.getLength()/step_length); //use int to round it to int steps
        for (int j = 1; j<=steps; j++)
        {
            actions.push(getActionForPoint(coords[i-1]+vector_between*double(j)/double(steps)));
        }

        actions.push(getActionForPoint(coords[i]));
    }

    auto end = std::chrono::high_resolution_clock::now();
    double duration = std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count();

    std::cout << "Square, elapsed time: " << duration / 1000.0 << "s" << std::endl;

    executeActions(actions);
}

void VirtualArm::executeParametric(float tmin, float tmax, float dt,
                                   std::function<float(float)> xt,
                                   std::function<float(float)> yt,
                                   std::function<float(float)> zt)
{
    std::vector<GMlib::Vector<float, 3>> coords;

    for(float t = tmin; t <= tmax; t += dt)
    {
        coords.push_back(GMlib::Vector<float, 3>(xt(t), yt(t), zt(t)));
    }

    executeCoords(coords);
}

void VirtualArm::setBaseAngle(GMlib::Angle angle)
{
    m_base->setAngle(angle,m_animate);

}

void VirtualArm::setShoulderAngle(GMlib::Angle angle)
{
    m_shoulder->setAngle(angle,m_animate);
   }

void VirtualArm::setElbowAngle(GMlib::Angle angle)
{
    m_elbow->setAngle(angle,m_animate);
    }

void VirtualArm::setWristAngle(GMlib::Angle angle)
{
    m_wrist->setAngle(angle,m_animate);
    }

void VirtualArm::setGripWidth(int space)
{
    m_gripper->setGripWidth(30-space);
}

void VirtualArm::setSpeed(int speed)
{
    m_base->setSpeed(speed);
    m_shoulder->setSpeed(speed);
    m_elbow->setSpeed(speed);
    m_wrist->setSpeed(speed);
    m_gripper->setSpeed(speed);
}

ArmAction VirtualArm::randomAction()
{
    //int baseAngle, elbowAngle, shoulderAngle, wristAngle;
    ArmAction return_action;
    std::vector<GMlib::Angle> angles;
    std::vector<GMlib::Vector<float,3>> poses;
    bool accept;

    do
    {
        m_randInt.set(0, 165); //for those angles with 0-165" freedom
        return_action.baseAngle     = GMlib::Angle(m_randInt.get());
        return_action.elbowAngle    = GMlib::Angle(m_randInt.get());
        m_randInt.set(0, 180); //fro those angles with 0-180" freedom
        return_action.shoulderAngle = GMlib::Angle(m_randInt.get());
        return_action.wristAngle    = GMlib::Angle(m_randInt.get());
        angles = {return_action.baseAngle,return_action.shoulderAngle,return_action.elbowAngle,return_action.wristAngle};
        getJointsPosesMat(angles,poses);
        accept = (poses[2][1]>0.0f &&
                  poses[3][1]>0.0f &&
                  poses[4][1]>0.0f ); //true if all are higher than environment
    } while (!accept);

    return return_action;
}

GMlib::Vector<float, 3> VirtualArm::randomPos()
{
    // No idea if this is correct, needs to find value of maxRadius.
    m_randInt.set(0, 400);
    float y = m_randInt.get();
    float phi = std::acos(y / m_maxRadius);
    m_randInt.set(-157079, 157079);
    float theta = m_randInt.get() / 100000.0f;
    float sinPhi = std::sin(phi);
    float x = m_maxRadius * std::sin(theta) * sinPhi;
    float z = m_maxRadius * std::cos(theta) * sinPhi;

    return GMlib::Vector<float, 3>(x, y, z);
}

void VirtualArm::randomStep()
{
    auto pos = randomPos();
    ArmState state = step(randomAction(), pos);

    std::cout << "Desired pos: " << pos << '\n';
    std::cout << "Actual pos: " << state.betweenFingersPos << '\n';
    std::cout << "Reward: " << state.reward << std::endl;
}

ArmState VirtualArm::step(ArmAction action, GMlib::Vector<float, 3> pos)
{
    //Simulation step for the AI framework
    //returns ArmState: joints' angles,
    //                  joints' positions,
    //                  eef position and
    //                  reward - length of the vector between actual eef pos and desired eef pos
    bool tempAnimate = m_animate;
    m_animate = false;

    doAction(action); //changes state of the arm with the respect to given action

    m_arm_controller_ptr->updateGUISliders(action.baseAngle,action.shoulderAngle,action.elbowAngle,action.wristAngle);

    m_animate = tempAnimate;

    return getState(pos);
}

std::vector<float> VirtualArm::step(std::vector<int> action, std::vector<float> desiredPos)
{
    if(action.size() != 4) {
        std::cerr << "Expected action.size() == " << 4 << ", but got " << action.size() << std::endl;
    }

    ArmState state = step(ArmAction{action[0], action[1], action[2], action[3]},
                          GMlib::Vector<float, 3>(desiredPos[0], desiredPos[1], desiredPos[2]));
    return std::vector<float>{
                static_cast<float>(state.baseAngle.getDeg()),
                static_cast<float>(state.shoulderAngle.getDeg()),
                static_cast<float>(state.elbowAngle.getDeg()),
                static_cast<float>(state.wristAngle.getDeg()),
                state.basePos[0], state.basePos[1], state.basePos[2],
                state.shoulderPos[0], state.shoulderPos[1], state.shoulderPos[2],
                state.elbowPos[0], state.elbowPos[1], state.elbowPos[2],
                state.wristPos[0], state.wristPos[1], state.wristPos[2],
                state.betweenFingersPos[0], state.betweenFingersPos[1], state.betweenFingersPos[2],
                state.reward
    };
}

GMlib::Vector<float, 3>  VirtualArm::getEEF()
{
    return m_gripper->getBetweenFingers(); // between fingers
    //return m_wrist->getGlobalPos();      // last joint before the gripper

    //return m_wrist->getGlobalPos() - wristHeightCorrection(); // last joint before the gripper with the correction 4.43f
}

double VirtualArm::findGamma(GMlib::Point<float, 3> &in_point)
{
    auto base_point = m_base->getGlobalPos();
    qDebug()<<"move to: "<<in_point[0]<<" "<<in_point[1]<<" "<<in_point[2]<<" ";
    qDebug()<<"base is: "<<base_point[0]<<" "<<base_point[1]<<" "<<base_point[2]<<" ";

    //point[0] - x
    //point[1] - y
    //point[2] - z
    //find gamma
    auto L1x = (in_point[0]-base_point[0]);
    auto L1z = (in_point[2]-base_point[2]);
    auto gamma_rad = M_PI - double (atan2(L1x,L1z)) ;

    GMlib::Angle test_ang = gamma_rad;
    qDebug()<<"angle before"<<test_ang.getDeg();

    // if goes out of scope - try to use valid angle
    if(gamma_rad< 0) {
        gamma_rad += M_PI;
    }
    else if(gamma_rad> M_PI) {
        gamma_rad -= M_PI;
    }

    test_ang = gamma_rad;
    qDebug()<<"angle after"<<test_ang.getDeg();

    return gamma_rad;
}

void VirtualArm::moveArmToPoint(GMlib::Point<float, 3> in_point)
{

    auto action = getActionForPoint(in_point);

    this->setBaseAngle(action.baseAngle);
    this->setShoulderAngle(action.shoulderAngle);
    this->setElbowAngle(action.elbowAngle);
    this->setWristAngle(action.wristAngle);
    m_arm_controller_ptr->updateGUISliders(action.baseAngle,action.shoulderAngle,action.elbowAngle,action.wristAngle);
    return;
}

ArmAction VirtualArm::getActionForPoint(GMlib::Point<float, 3> in_point)
{
    auto gamma_rad = findGamma(in_point);
    std::vector<GMlib::Angle> current_angles {gamma_rad,m_shoulder->getAngle(),m_elbow->getAngle(),m_wrist->getAngle()};
    std::vector<GMlib::Angle> new_angles;

    //if parallel - compute geometry based IK, else - compute jacobian
    new_angles = m_parallel_eef?GeometryIK(current_angles, in_point):JacobianIK(current_angles, in_point);
    return {new_angles[0],new_angles[1],new_angles[2],new_angles[3]};
}




void VirtualArm::getJointsPosesMat(std::vector<GMlib::Angle> &in_angles, std::vector<GMlib::Vector<float, 3>> &out_poses)
{
    //returns global poses of joints with input angles to output vector of poses
    //create local copy of joints, so that real ones will not be affected

    std::vector<GMlib::HqMatrix<float,3>> matricies;
    matricies.push_back(m_env->getMatrix());            //0
    matricies.push_back(m_base->getMatrix());           //1
    matricies.push_back(m_shoulder->getMatrix());       //2
    matricies.push_back(m_elbow->getMatrix());          //3
    matricies.push_back(m_wrist->getMatrix());          //4

    std::vector<GMlib::Vector<float,3>> axels;
    axels.push_back(m_env->getAxel());            //0
    axels.push_back(m_base->getAxel());           //1
    axels.push_back(m_shoulder->getAxel());       //2
    axels.push_back(m_elbow->getAxel());          //3
    axels.push_back(m_wrist->getAxel());          //4

    std::vector<GMlib::Angle> current_angles;
    //current_angles.push_back(m_env->getAngle());            //0
    current_angles.push_back(m_base->getAngle());           //1
    current_angles.push_back(m_shoulder->getAngle());       //2
    current_angles.push_back(m_elbow->getAngle());          //3
    current_angles.push_back(m_wrist->getAngle());          //4

    GMlib::Vector<float,3> new_axis;
    GMlib::HqMatrix<float,3> translate, rotation;
    GMlib::Angle new_angle;

    //clean output vector, if not empty
    if (out_poses.size()>0) out_poses.clear();

    //rotate base
    new_axis = matricies[1]*axels[1];
    new_angle = Mesh::getAngleDiff(current_angles[0],in_angles[0]);
    rotation = GMlib::HqMatrix<float,3> (new_angle, new_axis );
    translate = matricies[0]*matricies[1];
    matricies[1] = translate*rotation;
    out_poses.push_back(matricies[1].getCol(3));
    m_test_joints[0]->setMatrix(matricies[1]);

    //rotate shoulder
    new_axis = matricies[2]*axels[2];
    new_angle = Mesh::getAngleDiff(current_angles[1],in_angles[1]);
    rotation = GMlib::HqMatrix<float,3> (new_angle, new_axis );
    translate = matricies[1]*matricies[2];
    matricies[2] = translate*rotation;
    out_poses.push_back(matricies[2].getCol(3));
    m_test_joints[1]->setMatrix(matricies[2]);

    //rotate elbow
    new_axis = matricies[3]*axels[3];
    new_angle = Mesh::getAngleDiff(current_angles[2],in_angles[2]);
    rotation = GMlib::HqMatrix<float,3> (new_angle, new_axis );
    translate = matricies[2]*matricies[3];
    matricies[3] = translate*rotation;
    out_poses.push_back(matricies[3].getCol(3));
    m_test_joints[2]->setMatrix(matricies[3]);

    //rotate wrist
    new_axis = matricies[4]*axels[4];
    new_angle = Mesh::getAngleDiff(current_angles[3],in_angles[3]);
    rotation = GMlib::HqMatrix<float,3> (new_angle, new_axis );
    translate = matricies[3]*matricies[4];
    matricies[4] = translate*rotation;
    out_poses.push_back(matricies[4].getCol(3));
    m_test_joints[3]->setMatrix(matricies[4]);

    //eef
    auto offset = m_gripper->getOffset();
    GMlib::Vector<float,4> move_vec (offset, 0.0f, 0.0f, 1.0f);
    GMlib::HqMatrix<float,3> new_eef;
    new_eef.setCol((move_vec),3);
    matricies.push_back(matricies[4]*new_eef);
    out_poses.push_back(matricies[5].getCol(3));
    m_test_joints[4]->setMatrix(matricies[5]);

    return;
}

GMlib::Vector<float,3> VirtualArm::wristHeightCorrection()
{
    //correction to the height disposition due to object'c center with the respect to elbow
    auto correction = m_wrist->getPos(); //getpos - disposition from parent
    correction[1] = 0.0f;
    return m_elbow->getMatrixGlobal()*(m_wrist->getPos()-correction);
}

bool VirtualArm::isDone()
{
    // TODO: Check for collision, if so its done.
    // Check if the gripper is below the ground
    return getEEF()[1] < 0.0f;
}

ArmState VirtualArm::getState(GMlib::Vector<float, 3> desiredPos)
{
    return ArmState {
        m_base->getAngle(),
        m_shoulder->getAngle(),
        m_elbow->getAngle(),
        m_wrist->getAngle(),
        m_base->getCurrentPos(),
        m_shoulder->getCurrentPos(),
        m_elbow->getCurrentPos(),
        m_wrist->getCurrentPos(),
        getEEF(),
        calculateReward(desiredPos)
    };
}

bool VirtualArm::isPosLegal(GMlib::Vector<float, 3> pos)
{
    // not tested til the end..
    float phi = std::acos(pos[1] / m_maxRadius);
    float theta = std::acos(pos[2] / (m_maxRadius * std::sin(phi)));

    return pos.getLength() < m_maxRadius
            && (double(theta) > -M_PI/2 && double(theta) < M_PI/2)
            && (phi > 0.0f && double(phi) < M_PI);
}

void VirtualArm::doAction(ArmAction action)
{
    //std::cout << "VirtualArm::doAction(" << m_actionQueue.front().baseAngle
    //          << ", " << m_actionQueue.front().shoulderAngle
    //          << ", " << m_actionQueue.front().elbowAngle
    //          << ", " << m_actionQueue.front().wristAngle
    //          << ")" << std::endl;
    setBaseAngle(action.baseAngle);
    setShoulderAngle(action.shoulderAngle);
    setElbowAngle(action.elbowAngle);
    setWristAngle(action.wristAngle);
}

float VirtualArm::calculateReward(GMlib::Vector<float, 3> desiredPos)
{
    // Give negative reward if arm entered an illegal state
    if(isDone()) {
        return -10.0f;
    }

    // Distance between desired and actual pos
    float distance = (desiredPos - getEEF()).getLength();

    // Return 1 / distance. More reward the closer it is to the desiredPos
    if(distance <= 1.0f) {
        return 10.0f;
    }
    else {
        return 1.0f / distance;
    }
}

std::vector<GMlib::Angle> VirtualArm::JacobianIK(std::vector<GMlib::Angle> & input_angles, GMlib::Vector<float,3> goal_eef_pos)
{
    //IDEAS to TRY
    //IDEA 1
    //When could not find the solution try to start search from a random position several times
    //IDEA 2
    //When solution is with illegal angles, take that solution as a starting point for the calculations. and try one more time

    qDebug()<<"This is JACOBIAN!";
    auto max_iter = 10000;
    auto epsilon = 1.0f;
    auto distance = epsilon + 1.0f;
    auto iter = 0;
    auto step = 0.00001;

    //auto calc_DK = 0;

    std::vector<GMlib::Vector<float,3>> cur_joint_poses;
    GMlib::Vector<float,3> cur_eef_pos=getEEF();  //or can get from cur_joint_poses

    getJointsPosesMat(input_angles,cur_joint_poses); //cur joint pos from input angles
    //cur_eef_pos = cur_joint_poses[3];       //when last before gripper
    cur_eef_pos = cur_joint_poses[4]; //when between fingers

    //calc_DK++;

    while ( distance > epsilon and iter<=max_iter)
    {
        auto delta_angles = getDeltaAngles(goal_eef_pos,cur_eef_pos,cur_joint_poses);
        for (size_t i = 0; i<3; i++) { //for now when we have 3 angles
            input_angles[i+1] += delta_angles[i]*step;

//            //return angle to axeptable value// may be wrong //have to read about limits on possible values of jacobian
//            if (input_angles[i+1].getRad() < 0.0)
//                //input_angles[i+1] += M_PI;
//                input_angles[i+1] = 0.0;
//            else if (input_angles[i+1].getRad()> M_PI)
//                //input_angles[i+1] -= M_PI;
//                input_angles[i+1] = M_PI;
        }
        //pseudosimulate and get new cur_joint_poses to changed "input_angles" with getJointsPoses
        //cur_eef =calculate disstance between ( pseudomodel's eef coord and goal coord)
        getJointsPosesMat(input_angles,cur_joint_poses);
        //calc_DK++;
        //cur_eef_pos = cur_joint_poses[3];       //when last before gripper
        cur_eef_pos = cur_joint_poses[4]; //when between fingers

        distance = (cur_eef_pos-goal_eef_pos).getLength();

        iter++;
    }    

    //qDebug()<<"DK called "<<calc_DK<<" times";
    return {input_angles[0],input_angles[1],input_angles[2],input_angles[3]};
}

std::vector<GMlib::Angle> VirtualArm::GeometryIK(std::vector<GMlib::Angle> & input_angles, GMlib::Vector<float,3> goal_eef_pos)
{
    //computations based on Geometrical approach from lectures
    //used for computations with "parallel eef" - gripper parallel to the ground (XZ - plane)

    goal_eef_pos -= wristHeightCorrection(); //with correction of height due to model specific

    std::vector<GMlib::Vector<float,3>> pseudo_poses;
    getJointsPosesMat(input_angles,pseudo_poses);

    GMlib::Point<float,3> joint1_pos = pseudo_poses[1]; //joint1 position after the virtual turn of the base to the desired angle

    GMlib::Point<float,3> in_point_proj = goal_eef_pos; //projection of desired pos on XZ on the y level of joint1
    in_point_proj[1]=joint1_pos[1];

    GMlib::Point<float,3> jo1_proj_on_eef = joint1_pos; //projection of joint1 pos on XZ on the y level of eef
    jo1_proj_on_eef[1]=goal_eef_pos[1];

    auto eef_to_p_jo1 = (jo1_proj_on_eef-goal_eef_pos).normalize(); //direction from eef to the projection on XZ of joint 1
    goal_eef_pos += double(m_gripper->getOffset())*eef_to_p_jo1; //subtruct from given pos of eef disposition to wrist in the direction to robot center on XZ plane

    auto S = (m_elbow->getGlobalPos()-m_shoulder->getGlobalPos()).getLength(); //from joint2 to joint1
    auto H = (m_wrist->getGlobalPos() - wristHeightCorrection()-m_elbow->getGlobalPos()).getLength(); //from joint3 to joint2
    auto L  = (goal_eef_pos-joint1_pos).getLength();          //from joint3 to joint1
    //auto L1 = (in_point_proj-joint1_pos).getLength();     //from projection of desired pos to joint1
    auto y0 = goal_eef_pos[1]-joint1_pos[1];

    //auto alpha  = double(acos(y0/L)+acos(-(S*S-H*H-L*L)/(2.0f*H*L)));   //for joint "end"
    auto betta  = double(acos((S*S+H*H-L*L)/(2.0f*S*H)));                 //for joint 3
    auto alpha2 = double(acos((H*H+L*L-S*S)/(2.0f*H*L)));
    auto alpha1 = double(acos(y0/L));
    //auto alpha1 = alpha - alpha2; //alpha1 = acos(L1/L);
    //auto alpha11 = double(acos(L1/L));

    auto tetta1  = M_PI - alpha2 - betta;
    auto tetta2  = M_PI/2.0  - alpha1;
    auto tetta   = tetta1+tetta2; //for joint2

    GMlib::Angle wrist_angle = input_angles[3];

    auto phi = M_PI/2.0 + (M_PI-tetta-betta);

    qDebug()<<"Thetta   = "<<GMlib::Angle(tetta).getDeg();
    qDebug()<<"Betta    = "<<GMlib::Angle(betta).getDeg();
    qDebug()<<"Pi-Betta = "<<GMlib::Angle(M_PI-betta).getDeg();
    qDebug()<<"Phi      = "<<GMlib::Angle(phi).getDeg();

    input_angles = {input_angles[0],tetta,M_PI-betta,phi};
    //to check
    getJointsPosesMat(input_angles,pseudo_poses);

    return {input_angles[0],input_angles[1],input_angles[2],input_angles[3]};
}

std::vector<GMlib::Angle> VirtualArm::getDeltaAngles(GMlib::Vector<float,3> & target_pos, GMlib::Vector<float,3> & cur_eef_pos, std::vector<GMlib::Vector<float,3>> & cur_joint_poses)
{
    GMlib::Matrix<float, 3, 3> Jt = getJacobianTranspose(cur_eef_pos, cur_joint_poses);
    GMlib::Vector<float,3> V = target_pos - cur_eef_pos;
    GMlib::Vector<float,3> delta_angles = Jt*V;
    std::vector<GMlib::Angle> delta_in_vec;
    for (size_t i=0;i<3;i++)
    {
        delta_in_vec.push_back(double(delta_angles[int(i)]));
    }
    return delta_in_vec;
}

GMlib::Matrix<float, 3, 3> VirtualArm::getJacobianTranspose(GMlib::Vector<float,3> & cur_eef_pos, std::vector<GMlib::Vector<float,3>> & cur_joint_poses)
{
    //we do it for 3 joints only as we assume that base joint has already been rotated to the angle which is in one plane with the goal coordinates
    GMlib::Vector<float,3> J_A = (m_shoulder->getMatrixGlobal()*m_shoulder->getAxel()) ^ (cur_eef_pos - cur_joint_poses[1]);
    GMlib::Vector<float,3> J_B = (   m_elbow->getMatrixGlobal()*   m_elbow->getAxel()) ^ (cur_eef_pos - cur_joint_poses[2]);
    GMlib::Vector<float,3> J_C = (   m_wrist->getMatrixGlobal()*   m_wrist->getAxel()) ^ (cur_eef_pos - cur_joint_poses[3]);

    GMlib::Matrix<float, 3, 3> J;
    J.setCol(J_A,0);
    J.setCol(J_B,1);
    J.setCol(J_C,2);
//    //Transpose
//    //GMlib::Matrix<float, 3, 3>  J_T = J.getTransposed(); //this did not work, so do it manually
    GMlib::Matrix<float, 3, 3>  J_T;
    J_T.setCol(J.getRow(0),0);
    J_T.setCol(J.getRow(1),1);
    J_T.setCol(J.getRow(2),2);

    return J_T;
}

void VirtualArm::showJoints(bool shouldShow)
{
    for(size_t i = 0; i < m_test_joints.size(); i++) {
        m_test_joints[i]->setVisible(shouldShow);
    }
}

void VirtualArm::showPencil(bool shouldShow)
{
    m_gripper->showPencil(shouldShow);
}

void VirtualArm::drawSquare()
{
    //predefined square for testing
    std::vector<GMlib::Vector<float, 3> > coords;
    coords.push_back({250,40,-50});
    coords.push_back({250,40,50});
    coords.push_back({150,40,50});
    coords.push_back({150,40,-50});
    coords.push_back({250,40,-50});


//    Alphabet alf;
//    //coords = alf.getLetterCoordinates('h',{150.0f,50.0f,0.0f},0.5f);
//    coords = alf.getTextCoordinates("hello",{200.0f,45.0f,-150.0f},1.0f);
    executeCoords(coords);
}

void VirtualArm::drawInfinity()
{
    // Draw infinity symbol
    std::function<float(float)> xt = [](float t){return (100*std::cos(t)*std::sin(t))/(std::pow(std::sin(t),2)+1)+200;};
    std::function<float(float)> yt = [](float t){return 40;};
    std::function<float(float)> zt = [](float t){return (100*std::cos(t))/(std::pow(std::sin(t),2)+1);};
    executeParametric(0.0f, 2*M_PI+0.2f, 0.05f, xt, yt, zt);
//    drawFlower();
}

void VirtualArm::drawSpiral()
{
    // Draw a spiral
    std::function<float(float)> xt = [](float t){return((12*M_PI-t)*std::cos(t)+200); };
    std::function<float(float)> yt = [](float t){return 2 * t + 50; };
    std::function<float(float)> zt = [](float t){return((12*M_PI-t)*std::sin(t)); };
    executeParametric(0.0f, 10*M_PI, 0.15f, xt, yt, zt);
}

void VirtualArm::drawFlower()
{
    std::function<float(float)> xt = [](float t){return 75.0f*std::sin(4.0f*t)*std::cos(t)+200.0f; };
    std::function<float(float)> yt = [](float t){return 50; };
    std::function<float(float)> zt = [](float t){return 75.0f*std::sin(4.0f*t)*std::sin(t); };
    executeParametric(0.0f, 2*M_PI+0.2f, 0.05f, xt, yt, zt);
}

void VirtualArm::drawText(std::string text)
{
    Alphabet alf;
    //coords = alf.getLetterCoordinates('h',{150.0f,50.0f,0.0f},0.5f);
    auto coords = alf.getTextCoordinates(text,{200.0f,50.0f,-150.0f},1.0f);
    executeCoords(coords);
}

///////////////////////////////////////////deprecated
/// saved for testing


void VirtualArm::getJointsPoses(std::vector<GMlib::Angle> &in_angles, std::vector<GMlib::Vector<float, 3>> &out_poses)
{
    //returns global poses of joints with input angles to output vector of poses
    //create local copy of joints, so that real ones will not be affected

    std::vector<Mesh> copy_meshes;
    copy_meshes.push_back(*m_base);
    copy_meshes.push_back(*m_shoulder);
    copy_meshes.push_back(*m_elbow);
    copy_meshes.push_back(*m_wrist);
    copy_meshes.push_back(m_gripper->getLFinger());
    copy_meshes.push_back(m_gripper->getLKnuckle());
    copy_meshes.push_back(m_gripper->getRFinger());
    copy_meshes.push_back(m_gripper->getRKnuckle());


    if (out_poses.size()>0) out_poses.clear();

    GMlib::HqMatrix<float,3> new_pose = m_env->getMatrixGlobal()*copy_meshes[0].getMatrix();
    copy_meshes[0].setMatrix(new_pose);
    copy_meshes[0].setAngle(in_angles[0],false);
    out_poses.push_back(copy_meshes[0].getMatrix().getCol(3));
    m_test_joints[0]->setMatrix(out_poses.back());

    new_pose = copy_meshes[0].getMatrix()*copy_meshes[1].getMatrix();
    copy_meshes[1].setMatrix(new_pose);
    copy_meshes[1].setAngle(in_angles[1],false);
    out_poses.push_back(copy_meshes[1].getMatrix().getCol(3));
    m_test_joints[1]->setMatrix(out_poses.back());

    new_pose = copy_meshes[1].getMatrix()*copy_meshes[2].getMatrix();
    copy_meshes[2].setMatrix(new_pose);
    copy_meshes[2].setAngle(in_angles[2],false);
    out_poses.push_back(copy_meshes[2].getMatrix().getCol(3));
    m_test_joints[2]->setMatrix(out_poses.back());

    new_pose = copy_meshes[2].getMatrix()*copy_meshes[3].getMatrix();
    copy_meshes[3].setMatrix(new_pose);
    copy_meshes[3].setAngle(in_angles[3],false);
    out_poses.push_back(copy_meshes[3].getMatrix().getCol(3));
    m_test_joints[3]->setMatrix(out_poses.back());

    //before that everything is smooth, here some tricks/hardcoding
    //same as between fingers
//    auto offset = m_gripper->getOffset();
//    GMlib::HqMatrix<float,3> left_f_mat = copy_meshes[5].getMatrix();
//    auto left_f_pos = left_f_mat.getCol(3);
//    left_f_pos[0]=left_f_pos[0]+offset;

//    GMlib::HqMatrix<float,3> right_f_mat = copy_meshes[7].getMatrix();
//    auto right_f_pos = right_f_mat.getCol(3);
//    right_f_pos[0]=right_f_pos[0]+offset;

//    new_pose = left_f_mat;
//    new_pose.setCol((left_f_pos-(left_f_pos-right_f_pos)/2.0),3);

//    new_pose = copy_meshes[3].getMatrix()*new_pose;
//    copy_meshes[4].setMatrix(new_pose);
//    out_poses.push_back(copy_meshes[4].getMatrix().getCol(3));
//    m_test_joints[4]->setMatrix(out_poses.back());


    auto offset = 72.0f;


    GMlib::HqMatrix<float,3> ident_mat;

    GMlib::Vector<float,4> move_vec (offset, 0.0f, 0.0f, 1.0f);

    new_pose = ident_mat;
    new_pose.setCol((move_vec),3);

    new_pose = copy_meshes[3].getMatrix()*new_pose;
    copy_meshes[4].setMatrix(new_pose);
    out_poses.push_back(copy_meshes[4].getMatrix().getCol(3));
    m_test_joints[4]->setMatrix(out_poses.back());


    return;
}
