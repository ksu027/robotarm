#ifndef MESHVISUALIZER_H
#define MESHVISUALIZER_H

#include <scene/gmvisualizer.h>
#include <opengl/gmopengl.h>

#include <opengl/bufferobjects/gmvertexbufferobject.h>
#include <opengl/bufferobjects/gmindexbufferobject.h>
#include <opengl/gmprogram.h>
#include <opengl/gmtexture.h>

#include "texture.h"

namespace GMlib {
    class Camera;
    class Renderer;
    class DefaultRenderer;
    class SceneObject;
}

class MeshVisualizer : public GMlib::Visualizer
{
public:
    MeshVisualizer();
    MeshVisualizer(const MeshVisualizer& copy);
    ~MeshVisualizer() override;

    Visualizer* makeCopy() const override;

    void render(const GMlib::SceneObject*, const GMlib::DefaultRenderer*) const override;

    void setupGeometry(std::vector<GLfloat>& vs, std::vector<unsigned int>& is);
    void setTexture(Texture* tex);

private:
    GMlib::GL::Program m_prog;

    GMlib::GL::VertexBufferObject m_vbo;
    GMlib::GL::IndexBufferObject m_ibo;

    Texture* m_tex;

    int m_no_elements;
};

#endif // MESHVISUALIZER_H
