#include "realarm.h"

#include <iostream>

#include <QDebug>

RealArm::RealArm()
    : m_ser_con_pnt(nullptr), m_speed(300), m_execute_arm(false)
{

}

RealArm::~RealArm()
{
//    std::cout << "Real arm deleted" << std::endl;
}

void RealArm::setup()
{
}

bool RealArm::isReal()
{
    return true;
}

void RealArm::setBaseAngle(GMlib::Angle angle)
{
    if (m_execute_arm)
    {
        QByteArray command = QByteArray("#0 P") + QByteArray::number(angleToP(/*180 - */angle.getDeg(), 0)) + QByteArray(" S") + QByteArray::number(m_speed) + QByteArray(" \r\n");;
        m_ser_con_pnt->write(command);
    }
}

void RealArm::setShoulderAngle(GMlib::Angle angle)
{
    if (m_execute_arm)
    {
        QByteArray command = QByteArray("#1 P") + QByteArray::number(angleToP(angle.getDeg(), 1)) + QByteArray(" S") + QByteArray::number(m_speed) + QByteArray(" \r\n");
        m_ser_con_pnt->write(command);
    }
}

void RealArm::setElbowAngle(GMlib::Angle angle)
{
    if (m_execute_arm)
    {
        QByteArray command = QByteArray("#2 P") + QByteArray::number(angleToP(angle.getDeg(), 2)) + QByteArray(" S") + QByteArray::number(m_speed) + QByteArray(" \r\n");
        m_ser_con_pnt->write(command);
    }
}

void RealArm::setWristAngle(GMlib::Angle angle)
{
    if (m_execute_arm)
    {
        QByteArray command = QByteArray("#3 P") + QByteArray::number(angleToP(angle.getDeg(), 3)) + QByteArray(" S") + QByteArray::number(m_speed) + QByteArray(" \r\n");;
        m_ser_con_pnt->write(command);
    }
}

void RealArm::setGripWidth(int space)
{
    if (m_execute_arm)
    {
        QByteArray command = QByteArray("#4 P") + QByteArray::number(spaceToP(space)) + QByteArray(" S") + QByteArray::number(m_speed) + QByteArray(" \r\n");;
        m_ser_con_pnt->write(command);
    }
}

void RealArm::setSpeed(int speed)
{
    m_speed = speed;
}

void RealArm::shouldMove(const int& in_status)
{
    m_execute_arm=in_status==0?false:true;
}

int RealArm::angleToP(const float &in_angle, const int& in_joint_num)
{
    int out_p = 750;
    switch (in_joint_num) {                               //(max_p-min_p)/freedom_angle*input_angle+min_p
//        case 0 : out_p =int(10.0f*in_angle)+700;   break; //(2500-700)/180*in_angle+700
//        case 1 : out_p =int(9.167f*in_angle)+650;  break; //(2300-650)/180*in_angle+650
//        case 2 : out_p =int(in_angle*9.375f)+650;  break; //(2150-650)/160*in_angle+650
//        case 3 : out_p =int(in_angle*10.0f)+600;   break; //(2400-600)/180*in_angle+600
        case 0 : out_p =int((2500.0f-750.0f)/165.0f*in_angle)+750;   break;
        case 1 : out_p =int((2350.0f-660.0f)/180.0f*in_angle)+660;  break;
        case 2 : out_p =int((2150.0f-640.0f)/165.0f*in_angle)+640;  break;
        case 3 : out_p =int((2390.0f-590.0f)/180.0f*in_angle)+590;   break;
        //case 3 : out_p =int((2400.0f-570.0f)/180.0f*in_angle)+570;   break;
    }
    qDebug()<<out_p;
    return out_p;
//            int(in_angle*9.72f)+750;
}

float RealArm::pToAngle(const int &in_p)
{
    return (in_p-750.0f)/9.72f;
}

int RealArm::spaceToP(const float &in_space)
{
    /*
     * min signal 500
     * max signal 1700
     * min slider value 0
     * max slider value 30
     * 1700 - (1700-500)/(30-0)*in_space */
    return 1700-int(in_space*40);
}

void RealArm::connectToArm(const QString & port, const int & baud,const int & data_bits,const int & stop_bits,const int & parity,const int & flow)
{
    if (m_ser_con_pnt == nullptr)
        m_ser_con_pnt = new SerialConnection();

    m_ser_con_pnt->SetUpConnection(port, baud, QSerialPort::DataBits(data_bits), QSerialPort::StopBits(stop_bits), QSerialPort::Parity(parity), QSerialPort::FlowControl(flow));
}
