#include "meshvisualizer.h"

#include <scene/camera/gmcamera.h>
#include <scene/render/gmdefaultrenderer.h>

using namespace GMlib;

MeshVisualizer::MeshVisualizer()
    : m_no_elements(0)
{
    m_prog.acquire("textured_mesh");

    m_vbo.create();
    m_ibo.create();

    //    std::cout << "Created new MeshVisualizer" << std::endl;
}

MeshVisualizer::MeshVisualizer(const MeshVisualizer &copy)
    : GMlib::Visualizer(copy)
{
    // copy
}

MeshVisualizer::~MeshVisualizer()
{
    // TODO: Clean up
//    std::cout << "Deleted MeshVisualizer" << std::endl;
}

GMlib::Visualizer *MeshVisualizer::makeCopy() const
{
    return new MeshVisualizer(*this);
}

void MeshVisualizer::render(const GMlib::SceneObject *obj, const GMlib::DefaultRenderer *renderer) const
{
//    std::cout << "MeshVisualizer::render()" << std::endl;

    // Get camera from renderer
    const Camera* cam = renderer->getCamera();

    // Get model-view and projection matrices
    const HqMatrix<float, 3> &mvmat = obj->getModelViewMatrix(cam);
    const HqMatrix<float, 3> &pmat = obj->getProjectionMatrix(cam);

    // Set display mode, shaded/wireframe
    glSetDisplayMode();

    // Use shader program
    m_prog.bind(); {
        // Set model-view and mvp uniforms
        m_prog.uniform("u_mvmat", mvmat);
        m_prog.uniform("u_mvpmat", pmat * mvmat);

        // Set light uniforms
        m_prog.bindBufferBase("DirectionalLights", renderer->getDirectionalLightUBO(), 0);
        m_prog.bindBufferBase("PointLights", renderer->getPointLightUBO(), 1);
        m_prog.bindBufferBase("SpotLights", renderer->getSpotLightUBO(), 2);

        // Set material uniforms
        const Material& m = obj->getMaterial();
        m_prog.uniform("u_mat_amb", m.getAmb());
        m_prog.uniform("u_mat_dif", m.getDif());
        m_prog.uniform("u_mat_spc", m.getSpc());
        m_prog.uniform("u_mat_shi", m.getShininess());

        m_prog.uniform("u_tex", 0);
        m_tex->useTexture(GL_TEXTURE0);

        GL::AttributeLocation vert_loc = m_prog.getAttributeLocation("in_vertex");
        GL::AttributeLocation tex_loc = m_prog.getAttributeLocation("in_tex_coord");
        GL::AttributeLocation normal_loc = m_prog.getAttributeLocation("in_normal");

        m_vbo.bind();
        m_vbo.enable(vert_loc, 3, GL_FLOAT, GL_FALSE, sizeof(GL::GLVertexNormalTex2D), reinterpret_cast<const GLvoid*>(0x0));
        m_vbo.enable(tex_loc, 2, GL_FLOAT, GL_FALSE, sizeof(GL::GLVertexNormalTex2D), reinterpret_cast<const GLvoid*>(sizeof(GL::GLVertex)));
        m_vbo.enable(normal_loc, 3, GL_FLOAT, GL_TRUE, sizeof(GL::GLVertexNormalTex2D), reinterpret_cast<const GLvoid*>(sizeof(GL::GLVertexTex2D)));

        m_ibo.bind();
        m_ibo.drawElements(GL_TRIANGLES, m_no_elements, GL_UNSIGNED_INT, reinterpret_cast<const GLvoid*>(0x0));
        m_ibo.unbind();

        glBindTexture(GL_TEXTURE_2D, 0);
        m_vbo.disable(normal_loc);
        m_vbo.disable(vert_loc);
        m_vbo.unbind();
    } m_prog.unbind();
}

void MeshVisualizer::setupGeometry(std::vector<GLfloat> &vs, std::vector<unsigned int>& is)
{
    m_vbo.bind();
    m_vbo.bufferData(vs.size() * sizeof(GLfloat), &vs[0], GL_STATIC_DRAW);
    m_vbo.unbind();

    m_ibo.bind();
    m_no_elements = is.size();
    m_ibo.bufferData(is.size() * sizeof(GLuint), &is[0], GL_STATIC_DRAW);
    m_ibo.unbind();
}

void MeshVisualizer::setTexture(Texture* tex)
{
    m_tex = tex;
}
