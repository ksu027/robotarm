#include "armcontroller.h"

#include "virtualarm.h"
#include "robotarm.h"
#include "realarm.h"

#include "../application/window.h"

#include <QTimerEvent>
#include <QQuickItem>

ArmController::ArmController()
    : QObject(), m_virtualArm(nullptr), m_realArm(nullptr)
{

}

ArmController::~ArmController()
{
}

void ArmController::setUpSignals(Window &window)
{
	// Slots
    connect( window.rootObject(), SIGNAL(arm1SliderMoved(int)),     this, SLOT(setShoulderAngle(int)) );
    connect( window.rootObject(), SIGNAL(arm2SliderMoved(int)),     this, SLOT(setElbowAngle(int)) );
    connect( window.rootObject(), SIGNAL(arm3SliderMoved(int)),     this, SLOT(setWristAngle(int)) );
    connect( window.rootObject(), SIGNAL(baseSliderMoved(int)),     this, SLOT(setBaseAngle(int)) );
    connect( window.rootObject(), SIGNAL(handSpaceSliderMoved(int)),this, SLOT(setGripWidth(int)) );
    connect( window.rootObject(), SIGNAL(speedSpinChanged(int)),    this, SLOT(setSpeed(int)) );
    connect( window.rootObject(), SIGNAL(shouldGripperTrack(bool)),  this, SLOT(shouldGripperTrack(bool)));
    connect( window.rootObject(), SIGNAL(shouldEefParallel()),  this, SLOT(shouldEefParallel()));
    connect( window.rootObject(), SIGNAL(moveRealArmChanged(int)),  this, SLOT(shouldRealArmsMove(int)) );
    connect( window.rootObject(), SIGNAL(connectToArm( QString, int, int, int, int, int )),  this, SLOT(connectToArm( QString, int, int, int, int, int )) );
    connect( window.rootObject(), SIGNAL(executeActions(QVariant)), this, SLOT(executeActions(QVariant)));
    connect( window.rootObject(), SIGNAL(randomStepArm()), this, SLOT(randomStepArm()));
    connect( window.rootObject(), SIGNAL(drawSquareArm()), this, SLOT(drawSquareArm()));
    connect( window.rootObject(), SIGNAL(drawParametricArm()), this, SLOT(drawParametricArm()));
    connect( window.rootObject(), SIGNAL(showJoints(bool)), this, SLOT(showJoints(bool)));
    connect( window.rootObject(), SIGNAL(showPencil(bool)), this, SLOT(showPencil(bool)));
    connect( window.rootObject(), SIGNAL(drawSpiralArm()), this, SLOT(drawSpiralArm()));
    connect( window.rootObject(), SIGNAL(drawText(QString)), this, SLOT(drawText(QString)));

	// Signals
    connect( this,SIGNAL(baseSliderChange(QVariant)),window.rootObject(), SLOT(baseSliderChanged(QVariant)));
    connect( this,SIGNAL(arm1SliderChange(QVariant)),window.rootObject(), SLOT(arm1SliderChanged(QVariant)));
    connect( this,SIGNAL(arm2SliderChange(QVariant)),window.rootObject(), SLOT(arm2SliderChanged(QVariant)));
    connect( this,SIGNAL(arm3SliderChange(QVariant)),window.rootObject(), SLOT(arm3SliderChanged(QVariant)));
    connect( this,SIGNAL(eefPosChanged(QVariant, QVariant, QVariant)),window.rootObject(), SLOT(eefPosChanged(QVariant, QVariant, QVariant)));
    connect( this,SIGNAL(clearPlots()), window.rootObject(), SLOT(clearPlots()));
}

void ArmController::cleanUp()
{
    m_virtualArm->cleanUp();
    // m_realArm->cleanUp();
}

void ArmController::updateGUISliders(int in_base_angle, int in_shoulder_angle, int in_elbow_angle, int in_wrist_angle)
{
    baseSliderChange(in_base_angle);
    arm1SliderChange(in_shoulder_angle);
    arm2SliderChange(in_elbow_angle);
    arm3SliderChange(in_wrist_angle);

    m_realArm->setBaseAngle(in_base_angle);
    m_realArm->setShoulderAngle(in_shoulder_angle);
    m_realArm->setElbowAngle(in_elbow_angle);
    m_realArm->setWristAngle(in_wrist_angle);
}

void ArmController::setBaseAngle(int angle)
{
    GMlib::Angle a(angle);
    m_virtualArm->setBaseAngle(a);
    m_realArm->setBaseAngle(a);
}

void ArmController::setShoulderAngle(int angle)
{
    GMlib::Angle a(angle);
    m_virtualArm->setShoulderAngle(a);
    m_realArm->setShoulderAngle(a);
}

void ArmController::setElbowAngle(int angle)
{
    GMlib::Angle a(angle);
    m_virtualArm->setElbowAngle(a);
    m_realArm->setElbowAngle(a);
}

void ArmController::setWristAngle(int angle)
{
    GMlib::Angle a(angle);
    m_virtualArm->setWristAngle(a);
    m_realArm->setWristAngle(a);
}

void ArmController::setGripWidth(int space)
{
    m_virtualArm->setGripWidth(space);
    m_realArm->setGripWidth(space);
}

void ArmController::shouldGripperTrack(bool shouldTrack)
{
    m_virtualArm->shouldGripperTrack(shouldTrack);
}

void ArmController::shouldEefParallel()
{
    m_virtualArm->switchEefParallel();
}

void ArmController::setSpeed(const int& speed)
{
    m_virtualArm->setSpeed(speed);
    m_realArm->setSpeed(speed);
}

void ArmController::shouldRealArmsMove(const int &in_status)
{
    m_realArm->shouldMove(in_status);
}

void ArmController::executeActions(QVariant coords)
{
    std::vector<GMlib::Vector<float, 3>> actions;
    QStringList coordList = QVariant(coords).value<QStringList>();
    for(int i = 0; i < coordList.size() - 2; i += 3) {
        actions.push_back(GMlib::Vector<float, 3>{coordList[i].toFloat(), coordList[i+1].toFloat(), coordList[i+2].toFloat()});
    }
    m_virtualArm->executeCoords(actions);
}

void ArmController::randomStepArm()
{
    m_virtualArm->randomStep();
}

void ArmController::drawSquareArm()
{
    //m_virtualArm->setParallel(false);
    m_virtualArm->drawSquare();
}

void ArmController::drawParametricArm()
{
    //m_virtualArm->setParallel(true);
    m_virtualArm->drawInfinity();
}

void ArmController::drawSpiralArm()
{
    m_virtualArm->drawSpiral();
}

void ArmController::drawText(QString text)
{
    m_virtualArm->drawText(text.toStdString());
}

void ArmController::showJoints(bool shouldShow)
{
    m_virtualArm->showJoints(shouldShow);
}

void ArmController::showPencil(bool shouldShow)
{
    m_virtualArm->showPencil(shouldShow);
}

void ArmController::connectToArm(const QString &port, const int &baud, const int &data_bits, const int &stop_bits, const int &parity, const int &flow)
{
    m_realArm->connectToArm(port, baud, data_bits, stop_bits, parity, flow);
}

void ArmController::timerEvent(QTimerEvent *e)
{
    e->accept();
}
