#ifndef JOINT_H
#define JOINT_H

#include <parametrics/surfaces/gmpsphere.h>

class Joint : public GMlib::PSphere<float> {
public:
    Joint(float radius, GMlib::Vector<float, 3> rotAxel);
    ~Joint() override;

    void setAngle(GMlib::Angle angle);
    void setSpeed(double speed);

    void pseudoSimulate(GMlib::Angle angle);

    GMlib::Angle angle() const { return m_currAngle; }
    double speed() const { return m_speed; }
    GMlib::Vector<float, 3> getAxel() {return m_rotAxel; }

protected:
    void localSimulate(double dt) override;

private:
    GMlib::Angle m_currAngle;
    GMlib::Angle m_targetAngle;
    double m_speed;
    GMlib::Vector<float, 3> m_rotAxel;
};

#endif // JOINT_H
