
#include <iostream>

#include "scenario.h"

// hidmanager
#include "hidmanager/defaulthidmanager.h"

// gmlib
#include <scene/light/gmpointlight.h>
#include <scene/sceneobjects/gmpathtrack.h>
#include <scene/sceneobjects/gmpathtrackarrows.h>
#include <scene/camera/gmcamera.h>
#include <scene/render/gmdefaultrenderer.h>

#include "application/window.h"

#include "arm/robotarm.h"
#include "arm/realarm.h"
#include "arm/virtualarm.h"

#include "loader/texturedmeshshader.h"

// qt
#include <QQuickItem>

#include <qserialportinfo.h>

#include <QDebug>

#include "arm/joint.h"

//#include "arm/joint.h"

using vec3 = GMlib::Vector<float, 3>;

template <typename T>
inline
std::ostream& operator<<(std::ostream& out, const std::vector<T>& v) {
  out << v.size() << std::endl;
  for(uint i=0; i<v.size(); i++) out << " " << v[i];
  out << std::endl;
  return out;
}


void Scenario::initializeScenario() {


  QSerialPortInfo spi;
  QList<QVariant> port_list_variant;
  auto ports = spi.availablePorts();
  for (auto & port :ports)
  {
        qDebug()<<port.portName();
        port_list_variant.append(port.portName());
  }

  //fill combo box with available ports
  sendPorts(port_list_variant);

  // Insert a light
  auto keyColor = GMlib::Color(255, 244, 229);
  auto backColor = GMlib::Color(255, 244, 229);
  auto fillColor = GMlib::Color(255, 244, 229);

  GMlib::PointLight *keyLight = new GMlib::PointLight(
              keyColor, keyColor, keyColor,
              GMlib::Point<float, 3>(-500.0f, 500.0f, 500.0f));
  keyLight->setAttenuation(1.0f, 0.003f, 0.001f);
  this->scene()->insertLight( keyLight, true );

  GMlib::PointLight *backLight = new GMlib::PointLight(
              backColor, backColor, backColor,
              GMlib::Point<float, 3>(500.0f, 500.0f, 500.0f));
  backLight->setAttenuation(0.3f, 0.0015f, 0.0005f);
  scene()->insertLight(backLight, true);

  GMlib::PointLight *fillLight = new GMlib::PointLight(
              fillColor, fillColor, fillColor,
              GMlib::Point<float, 3>(-500.0f, 500.0f, -500.0f));
  fillLight->setAttenuation(0.5f, 0.002f, 0.0006f);
  scene()->insertLight(fillLight, true);

  // Insert Sun
//  this->scene()->insertSun();
//  this->scene()->scaleDayLight(0.3);
//  this->scene()->setSunDirection(GMlib::Angle(-90));


  // Default camera parameters
  int init_viewport_size = 600;
  GMlib::Point<float,3>  init_cam_pos( 0.0f, 0.0f, 0.0f );
  GMlib::Vector<float,3> init_cam_dir( 0.0f, 0.0f, 1.0f );
  GMlib::Vector<float,3> init_cam_up ( 0.0f, 1.0f, 0.0f );


  //camera is set up in such way, that coordinates are like that:
  //        Y
  //        ^
  //        |
  //        |
  //        |
  //        |
  //        |
  //        ---------------> X
  //       /
  //      /
  //     /
  //   |/_Z
  //

  // Projection cam
  auto proj_rcpair = createRCPair("Projection");
  proj_rcpair.camera->set(init_cam_pos,init_cam_dir,init_cam_up);
  proj_rcpair.camera->setCuttingPlanes( 1.0f, 4000.0f );
  proj_rcpair.camera->rotateGlobal( GMlib::Angle(180), GMlib::Vector<float,3>( 0.0f, 1.0f, 0.0f ) );
  proj_rcpair.camera->translateGlobal( GMlib::Vector<float,3>( 0.0f, 200.0f, 1500.0f ) );
  scene()->insertCamera( proj_rcpair.camera.get() );
  proj_rcpair.renderer->reshape( GMlib::Vector<int,2>(init_viewport_size, init_viewport_size) );

  TexturedMeshShader ts; // Adds the texturedMeshShader to the shader manager.

  // Create the robot arm
  auto arm = std::make_shared<VirtualArm>(&m_armController);
  arm->setup(scene());
  arm->setSpeed(300);

  auto realArm = std::make_shared<RealArm>();
  realArm->setup();

  m_armController.setVirtualArm(std::move(arm));
  m_armController.setRealArm(std::move(realArm));
  
	//testing
  GMlib::Material saphire(GMlib::GMmaterial::sapphire());
  saphire.set(45.0);
  test_obj = std::make_shared<Joint>(20.0, vec3(1, 0, 0));
  test_obj->sample(32, 32, 1, 1);
  test_obj->toggleDefaultVisualizer();
  test_obj->setMaterial(saphire);
  this->scene()->insert(test_obj.get());
}

void Scenario::cleanupScenario()
{
    m_armController.cleanUp();

    scene()->remove(test_obj.get());
    test_obj.reset();
}

void Scenario::setUpSignals(Window& window)
{
    connect( &window, &Window::beforeRendering, this, &Scenario::callDefferedGL, Qt::DirectConnection );
    m_armController.setUpSignals(window);

    //testing signal
    connect( window.rootObject(), SIGNAL(testButPress()),  this, SLOT(testButPress()) );
    connect( window.rootObject(), SIGNAL(testIKButPress()),  this, SLOT(testIKButPress()) );
}

void Scenario::callDefferedGL() {

  GMlib::Array< const GMlib::SceneObject*> e_obj;
  this->scene()->getEditedObjects(e_obj);

  for(int i=0; i < e_obj.getSize(); i++)
    if(e_obj(i)->isVisible()) e_obj[i]->replot();
}

void Scenario::testButPress()
{
    auto eef_coord = m_armController.getVirtualArm()->getEEF();
    qDebug()<<"eef coord: ("<<eef_coord[0]<<","<<eef_coord[1]<<","<<eef_coord[2]<<")";
    test_obj->setMatrix(eef_coord);
}

void Scenario::testIKButPress()
{
    auto goal_point = test_obj->getGlobalPos();
    auto arm        = m_armController.getVirtualArm();
    arm->moveArmToPoint(goal_point);

}
